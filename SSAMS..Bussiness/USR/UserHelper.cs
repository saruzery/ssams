﻿using ACAD.TOOL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SSAMS.Bussiness;
using SSAMS.Bussiness._Common.Utility.Authorization;
using SSAMS.DAL;
using SSAMS.Tools;
using Tigem.Business;
using SSAMS.TOOL;

namespace SSAMS.Bussiness.USR
{
    public class UserHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            //jsonParam.NOtOrdered = true;

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;
            var time = DateTime.Now;

            var query = db.Users.Where(o => o.Role <= input.Context.User.Role ).AsQueryable();
            
            //jsonParam.NotOrdered = true;
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, o => new
            {
               
                o.Id,
                o.Name,
                o.Surname,
                o.Email,
                o.Role
            }));

            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

      
        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            if (input.Context.User.Role >= RoleEnum.Manager && db.Users.Any(o => o.Id == jsonParam.Id && o.Role <= input.Context.User.Role))
            {
                var user = db.Users.Where(o => o.Id == jsonParam.Id).Select(o => new { o.Id, o.Name, o.Surname, o.Email, o.Role, o.CreatedDate, o.ModifiedDate }).SingleOrDefault();
                result.Data = JsonConvert.SerializeObject(user);
                result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            }
            else
            {
                result.Message = "Access Denied!";
                result.SetType(ServiceResultType.FAIL);
            }


            return result;
        }
        /*
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (User)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            if (input.Context.User.Role >= RoleEnum.Manager && input.Context.User.Role >= jsonParam.Role)
            {
                jsonParam.ModifiedDate = DateTime.Now;
              //  jsonParam.Password = SecurityFunctions.GetSHA1HashData(jsonParam.Password);
                if (db.Users.Any(o => o.Id == jsonParam.Id))
                {
                    db.Entry(jsonParam).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    jsonParam.CreatedDate = DateTime.Now;
                    db.Users.Add(jsonParam);
                }
                db.SaveChanges();
                result.Data = JsonConvert.SerializeObject(jsonParam.Id);
                result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            }
            else
            {
                result.Message = "ڕێگەپێنەدراویت";
                result.SetType(ServiceResultType.FAIL);
            }


            return result;
        }
        */
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (User)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var errors = db.GetEntityValidationErrors(jsonParam);
            jsonParam.Password = SecurityFunctions.GetSHA1HashData(jsonParam.Password);
            var item = db.Users.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.Name = jsonParam.Name;
                item.Surname = jsonParam.Surname;
                item.Email = jsonParam.Email;
                item.Password = jsonParam.Password;
                item.Role = jsonParam.Role;
                item.ModifiedDate = DateTime.Now;
            }
            else
            {
                jsonParam.ModifiedDate = DateTime.Now;
                jsonParam.CreatedDate = DateTime.Now;
                db.Users.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> SignIn(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Email = (string)null, Pass = (string)null, RememberMe=false };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var time = DateTime.Now;

            if (!string.IsNullOrEmpty(jsonParam.Email) && !string.IsNullOrEmpty(jsonParam.Pass))
            {
                var user = db.Users.SingleOrDefault(o => o.Email == jsonParam.Email);
                if (user != null)
                {
                    if (user.Password == SecurityFunctions.GetSHA1HashData(jsonParam.Pass))
                    {
                        var vsData = new CookieData(user.Id, user.Name, false, user.Role);
                        CustomAuthenticationService.SignIn(vsData, jsonParam.RememberMe);
                          //  .SessionHelper.Login(vsData);
                        var request = HttpContext.Current.Request;
                        user.UserActivities.Add(new SSAMS.DAL.UserActivity { BrowserName = request.Browser.Browser, BrowserPlatform = getOS(request), IPAddress = request.UserHostAddress, VisitDate = time, });
                        db.SaveChanges();

                        result.Message = "سەرکەوتوبویت";
                        result.SetType(ServiceResultType.SUCCESS);
                    }
                    else
                    {
                        result.Message = "ووشەی نهێنی هەڵەیە";
                        result.SetType(ServiceResultType.NOT_AUTHENTICATED);
                    }
                }
                else
                {
                    result.Message = "ئیمەیڵەکەت هەڵەیە";
                    result.SetType(ServiceResultType.NOT_VALID);
                }
            }
            else
            {
                result.Message = "   Please fill the blanks";
                result.SetType(ServiceResultType.NOT_VALID);
            }

            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id=0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var time = DateTime.Now;
            var user = input.Context.User;
            if (user.Role >= RoleEnum.Manager && db.Users.Any(o => o.Id == jsonParam.Id))
            {
                db.UserActivities.RemoveRange(db.UserActivities.Where(o=> o.UserId == jsonParam.Id));
                db.Users.Remove(db.Users.Single(o=> o.Id == jsonParam.Id));
                db.SaveChanges();
                result.SetType(ServiceResultType.SUCCESS);
            }
            else
            {
                result.Message = "Access Denied!";
                result.SetType(ServiceResultType.FAIL);
            }
            return result;
        }
        public static ServiceResult<object> LockOut(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Pass = (string)null};
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var time = DateTime.Now;
            var user = input.Context.User;

                var dbUser = db.Users.SingleOrDefault(o => o.Id == user.UserId);
                if (dbUser != null)
                {
                    if (dbUser.Password == SecurityFunctions.GetSHA1HashData(jsonParam.Pass))
                    {
                        CustomAuthenticationService.LockOut();
                        //  .SessionHelper.Login(vsData);
                        var request = HttpContext.Current.Request;
                        dbUser.UserActivities.Add(new SSAMS.DAL.UserActivity { BrowserName = request.Browser.Browser, BrowserPlatform = getOS(request), IPAddress = request.UserHostAddress, VisitDate = time, });
                        db.SaveChanges();

                        result.Message = "successfull";
                        result.SetType(ServiceResultType.SUCCESS);
                    }
                    else
                    {
                        result.Message = "Your password is wrong.";
                        result.SetType(ServiceResultType.NOT_AUTHENTICATED);
                    }
                }
                else
                {
                    result.Message = "Please enter correct username";
                    result.SetType(ServiceResultType.NOT_VALID);
                }
                
            return result;
        }

        public static ServiceResult<object> GetCurrentUserDb(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            //var jsonParam = (WebDataTableFilterView)null;
            //jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var db = input.Context.SSAMSEntities;
            var user = db.Users.Where(o => o.Id == input.Context.User.UserId).Select(o => new { o.Id, o.Name, o.Surname}).Single();
            result.Data = JsonConvert.SerializeObject(user);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> CurrentUser(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            //var jsonParam = (WebDataTableFilterView)null;
            //jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            
            var user = CustomAuthenticationService.GetAuthenticationCookieObject();
            result.Data = JsonConvert.SerializeObject(new { UserName = user.UserName, Role = user.Role});
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        static string getOS(HttpRequest request)
        {
            string os = null;
            if (request.UserAgent.IndexOf("Windows NT 5.1") > 0)
            {
                os = "Windows XP";
                return os;
            }
            else if (request.UserAgent.IndexOf("Windows NT 6.0") > 0)
            {
                os = "Windows Vista";
                return os;
            }
            else if (request.UserAgent.IndexOf("Windows NT 6.1") > 0)
            {
                os = "Windows 7";
                return os;
            }
            else if (request.UserAgent.IndexOf("Windows NT 6.2") > 0)
            {
                os = "Windows 8";
                return os;
            }
            else if (request.UserAgent.IndexOf("Windows NT 6.3") > 0)
            {
                os = "Windows 8.1";
                return os;
            }
            else if (request.UserAgent.IndexOf("Windows NT 10.0") > 0)
            {
                os = "Windows 10";
                return os;
            }
            else if (request.UserAgent.IndexOf("Linux") > 0)
            {
                os = "Linux";
                return os;
            }
            else if (request.UserAgent.IndexOf("X11") > 0)
            {
                os = "Linux";
                return os;
            }
            else if (request.UserAgent.IndexOf("Intel Mac OS X") > 0)
            {
                //os = "Mac OS or older version of Windows";
                os = "Intel Mac OS X";
                return os;
            }
            else
            {
                os = "You are using older version of Windows or Mac OS";
                return os;
            }
        }

    }
}
