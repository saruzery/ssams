﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Bussiness
{
    public class BusinessGlobals
    {

        public class TurkishDecimalFormatConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(decimal) || objectType == typeof(decimal?));
            }

            public override void WriteJson(JsonWriter writer, object value,
                                           JsonSerializer serializer)
            {
                if (value == null)
                {

                }
                else
                {
                    decimal val = Convert.ToDecimal(value);
                    writer.WriteValue(val.ToString("n2", new CultureInfo("tr-TR")));
                }

            }

            public override bool CanRead
            {
                get { return false; }
            }

            public override object ReadJson(JsonReader reader, Type objectType,
                                         object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }

        public static TurkishDecimalFormatConverter getTurkishDecimalFormatConverter()
        {
            return new TurkishDecimalFormatConverter();
        }

        //public static Pocos.Database contructNewPetaPocoDB(OldParameterBag bag)
        //{
        //    String connectionString = bag.getStringValue("ConnectionString");
        //    String providerName = bag.getStringValue("ProviderName");
        //    return new Pocos.Database(connectionString, providerName);
        //}

        //public static Pocos.Database contructNewPetaPocoDB(String connectionString, String providerName)
        //{
        //    return new Pocos.Database(connectionString, providerName);
        //}


        public static IsoDateTimeConverter getNewISODateTimeConverter()
        {
            return new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };
        }

        public static IsoDateTimeConverter getNewISODateConverter()
        {
            return new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" };
        }

        public static IsoDateTimeConverter getNewTurkisISODateConverter()
        {
            return new IsoDateTimeConverter() { DateTimeFormat = "dd.MM.yyyy" };
        }

        public static IsoDateTimeConverter getNewTurkisISODateTimeConverter()
        {
            return new IsoDateTimeConverter() { DateTimeFormat = "dd.MM.yyyy HH:mm:ss" };
        }
        /*
        public static IsoDateTimeConverter getIsoDateTimeConverter(ParameterBag input)
        {
            String format = input.Data.getString("DateFormat");

            if (String.IsNullOrEmpty(format))
            {
                return getNewISODateConverter();
            }
            else
            {
                return new IsoDateTimeConverter() { DateTimeFormat = format };
            }
        }
        */
    }
}
