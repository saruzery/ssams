﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class TeacherAssignmentHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam1 = new { IsDetail = false, UserId = (int?)0 };
            jsonParam1 = JsonConvert.DeserializeAnonymousType(param, jsonParam1);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);
            var jsonParam3 = new { IsDetail = false, GroupId = (int?)0 };
            jsonParam3 = JsonConvert.DeserializeAnonymousType(param, jsonParam3);
            var jsonParam4 = new { IsDetail = false, LectureId = (int?)0 };
            jsonParam4 = JsonConvert.DeserializeAnonymousType(param, jsonParam4);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.TeacherAssignment.AsQueryable();
            if (jsonParam1 != null && jsonParam1.UserId.HasValue)
                query = query.Where(o => o.UserId == jsonParam1.UserId.Value);
            if (jsonParam2 != null && jsonParam2.ClassId.HasValue)
                query = query.Where(o => o.ClassId == jsonParam2.ClassId.Value);
            if (jsonParam3 != null && jsonParam3.GroupId.HasValue)
                query = query.Where(o => o.GroupId == jsonParam3.GroupId.Value);
            if (jsonParam4 != null && jsonParam4.LectureId.HasValue)
                query = query.Where(o => o.LectureId == jsonParam4.LectureId.Value);
            Expression<Func<TeacherAssignment, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.UserId,
                    User = new { o.User.Name, o.User.Surname },
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.GroupId,
                    Group = new { o.Group.GroupName },
                    o.LectureId,
                    Lecture = new { o.Lecture.LectureName }
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.UserId,
                    User = new { o.User.Name, o.User.Surname },
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.GroupId,
                    Group = new { o.Group.GroupName },
                    o.LectureId,
                    Lecture = new { o.Lecture.LectureName }
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.TeacherAssignment.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.UserId,
                User = new { o.User.Name, o.User.Surname },
                o.ClassId,
                Class = new { o.Class.ClassName },
                o.GroupId,
                Group = new { o.Group.GroupName },
                o.LectureId,
                Lecture = new { o.Lecture.LectureName }
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (TeacherAssignment)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var errors = db.GetEntityValidationErrors(jsonParam);

            var item = db.TeacherAssignment.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.UserId = jsonParam.UserId;
                item.ClassId = jsonParam.ClassId;
                item.GroupId = jsonParam.GroupId;
                item.LectureId = jsonParam.LectureId;
                item.UpdateDate = DateTime.Now;
            }
            else
            {
                jsonParam.UpdateDate = DateTime.Now;
                jsonParam.CreatedDate = DateTime.Now;
                db.TeacherAssignment.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            db.TeacherAssignment.Remove(db.TeacherAssignment.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "Successfull";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
