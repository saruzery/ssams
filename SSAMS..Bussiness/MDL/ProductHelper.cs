﻿using Newtonsoft.Json;
using SSAMS.DAL;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;

namespace SSAMS.Bussiness.MDL
{
    public class ProductHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, StoreId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);
            jsonParam.Order.Col = jsonParam.Order.Col ?? "CreatedDate";

            var db = input.Context.TSCSEntities;

            var query = db.Product.AsQueryable();
            var allCount = query.Count();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.Name.Contains(jsonParam.SearchText) || o.Code.Contains(jsonParam.SearchText));

            Expression<Func<Product, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
            {
                o.Id,
                o.Code,
                o.Name,
                o.Price,
                o.Description,
                o.ImageDirectory,
                o.CreatedDate,
                o.UpdateDate,
            };
            else
                obj = o => new
            {
                o.Id,
                o.Code,
                o.Name,
                o.Price,
                o.Description,
                o.ImageDirectory,
                o.CreatedDate,
                o.UpdateDate,
            };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj, allCount));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.TSCSEntities;

            var query = db.Product.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.Code,
                o.Name,
                o.Price,
                o.Description,
                o.ImageDirectory,
                o.CreatedDate,
                o.UpdateDate,
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (Product)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
           // var userid = input.Context.User.UserId;
            var db = input.Context.TSCSEntities;

            var item = db.Product.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.Code = jsonParam.Code;
                item.Name = jsonParam.Name;
                item.Price = jsonParam.Price;
                item.Description = jsonParam.Description;
                item.ImageDirectory = jsonParam.ImageDirectory;
                item.UpdateDate = DateTime.Now;
            }
            else
            {
                jsonParam.CreatedDate = DateTime.Now;
                db.Product.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "سەرکەوتوبویت";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.TSCSEntities;
            db.Product.Remove(db.Product.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "سەرکەوتوبویت";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
