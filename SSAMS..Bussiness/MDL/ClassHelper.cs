﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class ClassHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.Class.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.ClassName.Contains(jsonParam.SearchText));
            Expression<Func<Class, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.ClassName,
                    o.Description
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.ClassName,
                     o.Description
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> GetListByClassId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);


            var db = input.Context.SSAMSEntities;

            var query = db.ClassDetail.Where(o => o.ClassId == jsonParam.Id).Select(o => new
            {
                o.RoomName,
                o.Description
            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> GetClassByLectureId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Lecture.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
               // o.ClassId,
                o.Class.ClassName,
                o.ClassId,
                
            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Class.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.ClassName,
                o.Description
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Class = (Class)null, ClassDetail = (List<ClassDetail>)null };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var Class = jsonParam.Class;
            var ClassDetail = jsonParam.ClassDetail;

            var item = db.Class.Where(o => o.Id == Class.Id).SingleOrDefault();
            if (item != null)
            {

                item.Description = Class.Description;
                item.ClassName = Class.ClassName;
                item.UpdateDate = DateTime.Now;
                db.ClassDetail.RemoveRange(item.ClassDetail);
                item.ClassDetail = ClassDetail;

            }
            else
            {

                Class.ClassDetail = ClassDetail;
                Class.CreatedDate = DateTime.Now;
                Class.UpdateDate = DateTime.Now;

                db.Class.Add(Class);
            }
            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> GetRoomByClassId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.ClassDetail.Where(o => o.ClassId == jsonParam.Id).Select(o => new
            {
                 o.Id,
                o.Class.ClassName,
                o.ClassId,
                o.RoomName

            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);


            var db = input.Context.SSAMSEntities;
            // var passHash = SecurityFunctions.GetSHA1HashData(jsonParam.Pass);
            if (db.ClassDetail.Any(o => o.ClassId == jsonParam.Id))
            {
                db.ClassDetail.RemoveRange(db.ClassDetail.Where(o => o.ClassId == jsonParam.Id));
                var Class = db.Class.Single(o => o.Id == jsonParam.Id);

                db.Class.Remove(Class);

                db.SaveChanges();

                result.Message = "Successfull..  ";
                result.SetType(ServiceResultType.SUCCESS);
            }
            else
            {

                result.Message = "Failed..";
                result.SetType(ServiceResultType.FAIL);
            }


            return result;
        }
    }
}
