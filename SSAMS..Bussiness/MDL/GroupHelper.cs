﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;
using SSAMS.TOOL;

namespace SSAMS.Bussiness.MDL
{
    public class GroupHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0, IsExpired = false };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;
            //  var StatusCount = db.GroupDetail.Any(o => o.Status = 1);

            var query = db.Group.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.GroupName.Contains(jsonParam.SearchText));
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText2))
                query = query.Where(o => o.User.Name.Contains(jsonParam.SearchText2));
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText1))
                query = query.Where(o => o.GroupDetail.Select(p=> p.ClassName).Contains(jsonParam.SearchText1));

            var today = DateTime.Now;
            if (jsonParam2.IsExpired)
                query = query.Where(o => o.EndDateTime < today);
            else
                query = query.Where(o=> o.EndDateTime>=today);

            var userRole = input.Context.User.Role;
            var id = input.Context.User.UserId;

            if (userRole == SSAMS.DAL.RoleEnum.Teacher)
             {
                 query = query.Where(o => o.UserId == (id));
            }
            Expression<Func<Group, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.StartHour,
                    o.StartMinute,
                    o.EndDateTime,
                    o.EndHour,
                    o.EndMinute,
                    GroupDetail = o.GroupDetail.Select(t => new { t.Status }),
                    ClassName = o.GroupDetail.Select(s => new {s.ClassName }),
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.StartHour,
                    o.StartMinute,
                    o.EndDateTime,
                    o.EndHour,
                    o.EndMinute,
                    ClassName = o.GroupDetail.Select(s => new { s.ClassName }),
                    Attend = o.GroupDetail.Count(t => t.Status == Status.Attend),
                    Absence = o.GroupDetail.Count(t => t.Status == Status.Absence),
                    Permission = o.GroupDetail.Count(t => t.Status == Status.Permission),
                };

            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Group.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.LectureId,
                o.Lecture.LectureName,
                o.UserId,
                o.User.Name,
                o.User.Surname,
                o.GroupName,
                o.Description,
                o.StartDateTime,
                o.StartHour,
                o.StartMinute,
                o.EndDateTime,
                o.EndHour,
                o.EndMinute,
                GroupDetail = o.GroupDetail.Select(t => new { t.Status }),
                ClassName = o.GroupDetail.Select(s => new { s.ClassName }),
                

            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public ServiceResult<object> GetStatus(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            Status attend = Status.Attend;

            var query = db.GroupDetail.Where(o => o.Id == jsonParam.Id && o.Status == attend).Select(o => new { o.Status }
            ).ToList();
            var attendCount = query.Count();

            Status Absence = Status.Absence;
            var query1 = db.GroupDetail.Where(o => o.Id == jsonParam.Id && o.Status == Absence).Select(o => new { o.Status }
            ).ToList();
            var absenceCount = query1.Count();

            Status Permition = Status.Absence;
            var query2 = db.GroupDetail.Where(o => o.Id == jsonParam.Id && o.Status == Permition).Select(o => new { o.Status }
            ).ToList();
            var permitionCount = query2.Count();

            var str = JsonConvert.SerializeObject(attendCount);
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> GetByUserId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0, IsExpired = false };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.Group.AsQueryable();
            query = query.Where(o => o.UserId == input.Context.User.UserId);

            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.GroupName.Contains(jsonParam.SearchText));
            var today = DateTime.Now;
            if (jsonParam2.IsExpired)
                query = query.Where(o => o.EndDateTime < today);
            else
                query = query.Where(o => o.EndDateTime >= today);

            Expression<Func<Group, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.StartHour,
                    o.StartMinute,
                    o.EndDateTime,
                    o.EndHour,
                    o.EndMinute,
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.StartHour,
                    o.StartMinute,
                    o.EndDateTime,
                    o.EndHour,
                    o.EndMinute,

                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetListByGroupId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);


            var db = input.Context.SSAMSEntities;

            var query = db.GroupDetail.Where(o => o.GroupId == jsonParam.Id).Select(o => new
            {
                o.StudentId,
                o.Student.StudentName,
                o.Student.Surname,
                o.ClassName,
                o.Student.MotherName,
                o.Student.Phone1,
                o.Id,
                o.Status,
                 o.RoomName

            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Group = (Group)null, GroupDetail = (List<GroupDetail>)null };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var Group = jsonParam.Group;
            var GroupDetail = jsonParam.GroupDetail;

            var item = db.Group.Where(o => o.Id == Group.Id).SingleOrDefault();
            if (item != null)
            {
                item.EndDateTime = Group.EndDateTime;
                item.StartDateTime = Group.StartDateTime;
                item.Description = Group.Description;
                item.GroupName = Group.GroupName;
                item.UpdateDate = DateTime.Now;
                db.GroupDetail.RemoveRange(item.GroupDetail);
                item.GroupDetail = GroupDetail;

            }
            else
            {

                Group.GroupDetail = GroupDetail;
                Group.CreatedDate = DateTime.Now;
                Group.UpdateDate = DateTime.Now;

                db.Group.Add(Group);
            }
            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            // var passHash = SecurityFunctions.GetSHA1HashData(jsonParam.Pass);
            if (db.Group.Any(o => o.Id == jsonParam.Id))
            {
                db.GroupDetail.RemoveRange(db.GroupDetail.Where(o => o.GroupId == jsonParam.Id));
                var Group = db.Group.Single(o => o.Id == jsonParam.Id);

                db.Group.Remove(Group);

                db.SaveChanges();

                result.Message = "Successfull..  ";
                result.SetType(ServiceResultType.SUCCESS);
            }
            else
            {

                result.Message = "Failed..";
                result.SetType(ServiceResultType.FAIL);
            }
            return result;
        }
    }
}
