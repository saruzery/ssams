﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;
using SSAMS.TOOL;

namespace SSAMS.Bussiness.MDL
{
    public class DashboardHelper
    {
        /*
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.Group.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.GroupName.Contains(jsonParam.SearchText));

            Expression<Func<Group, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.EndDateTime
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.LectureId,
                    o.Lecture.LectureName,
                    o.UserId,
                    o.User.Name,
                    o.User.Surname,
                    o.GroupName,
                    o.Description,
                    o.StartDateTime,
                    o.EndDateTime

                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        */
       
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Group = (Group)null, GroupDetail = (List<GroupDetail>)null };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var Group = jsonParam.Group;
            var GroupDetail = jsonParam.GroupDetail;

            Group.GroupDetail = GroupDetail;
            Group.CreatedDate = DateTime.Now;
            Group.UpdateDate = DateTime.Now;

            db.Group.Add(Group);
            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0};
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
           // var passHash = SecurityFunctions.GetSHA1HashData(jsonParam.Pass);
            if (db.GroupDetail.Any(o => o.Id == jsonParam.Id))
            {
                db.GroupDetail.RemoveRange(db.GroupDetail.Where(o => o.GroupId == jsonParam.Id));
                var Group = db.Group.Single(o => o.Id == jsonParam.Id);
              
                db.Group.Remove(Group);

                db.SaveChanges();

                result.Message = "Successfull..  ";
                result.SetType(ServiceResultType.SUCCESS);
            }
            else
            {

                result.Message = "Failed..";
                result.SetType(ServiceResultType.FAIL);
            }
            return result;
        }
    }
}
