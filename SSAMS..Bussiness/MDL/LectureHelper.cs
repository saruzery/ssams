﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class LectureHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.Lecture.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.LectureName.Contains(jsonParam.SearchText));
            if (jsonParam2 != null && jsonParam2.ClassId.HasValue)
                query = query.Where(o => o.ClassId == jsonParam2.ClassId.Value);
            Expression<Func<Lecture, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.LectureName,
                    o.Description
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.LectureName,
                    o.Description
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Lecture.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.ClassId,
                o.LectureName,
                o.Description
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetByClassId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Lecture.Where(o => o.ClassId == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.Class.ClassName,
                o.ClassId,
                o.LectureName

            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }


        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (Lecture)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var errors = db.GetEntityValidationErrors(jsonParam);

            var item = db.Lecture.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.ClassId = jsonParam.ClassId;
                item.LectureName = jsonParam.LectureName;
                item.Description = jsonParam.Description;
                item.UpdateDate = DateTime.Now;
            }
            else
            {
                jsonParam.UpdateDate = DateTime.Now;
                jsonParam.CreatedDate = DateTime.Now;
                db.Lecture.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            db.Lecture.Remove(db.Lecture.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "Successfull";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
