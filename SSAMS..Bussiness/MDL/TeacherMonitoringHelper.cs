﻿using Newtonsoft.Json;
using SSAMS.DAL;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;

namespace SSAMS.Bussiness.MDL
{
    public class TeacherMonitoringHelper
    {

        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            var jsonParam3 = new { IsDetail = false, GroupId = (int?)0 };
            jsonParam3 = JsonConvert.DeserializeAnonymousType(param, jsonParam3);


            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.TeacherAssignment.AsQueryable();
           
            if (jsonParam2 != null && jsonParam2.ClassId.HasValue)
                query = query.Where(o => o.ClassId == jsonParam2.ClassId.Value);
            if (jsonParam3 != null && jsonParam3.GroupId.HasValue)
                query = query.Where(o => o.GroupId == jsonParam3.GroupId.Value);
           
            Expression<Func<TeacherAssignment, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.UserId,
                    User = new { o.User.Name, o.User.Surname },
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.GroupId,
                    Group = new { o.Group.GroupName },
                    
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.UserId,
                    User = new { o.User.Name, o.User.Surname },
                    o.ClassId,
                    Class = new { o.Class.ClassName },
                    o.GroupId,
                    Group = new { o.Group.GroupName },
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Attendance.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.ClassId,
                o.Class.ClassName,
                o.GroupId,
                o.Group.GroupName,
                o.LectureId,
                o.Lecture.LectureName,
                o.StartTime,
                o.EndTime,
                o.CreatedDate,
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

    }
}
