﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class ReportHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.Class.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.ClassName.Contains(jsonParam.SearchText));
            Expression<Func<Class, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.ClassName
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.ClassName
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetClassByLectureId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Lecture.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
               // o.ClassId,
                o.Class.ClassName,
                o.ClassId,
                
            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Class.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.ClassName
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (Class)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var item = db.Class.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {

                item.ClassName = jsonParam.ClassName;
                item.UpdateDate = DateTime.Now;
            }
            else
            {
                jsonParam.UpdateDate = DateTime.Now;
                jsonParam.CreatedDate = DateTime.Now;
                db.Class.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            db.Class.Remove(db.Class.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "Successfull";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
