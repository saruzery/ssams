﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;
using SSAMS.Tools;

namespace SSAMS.Bussiness.MDL
{
    public class ShopProfileHelper
    {

        
         public static ServiceResult<object> GetShopProfile(ParameterBag input)
       {
           
                  var result = new ServiceResult<object>();
                  var param = input.Data as string;
                  var db = input.Context.TSCSEntities;

                  var data = db.ShopProfile.Select(o => new
                  {
                      Id = o.Id,
                      ShopName = o.ShopName,
                      Phone1 = o.Phone1,
                      Phone2 = o.Phone2,
                      Address = o.Address,
                      Email = o.Email,
                      Website = o.Website,
                      FacebookLink = o.FacebookLink,
                      StartInvoiceId = o.StartInvoiceId,
                      LogoDirectory = o.LogoDirectory,
                      CreatedDate = o.CreatedDate,
                      UpdateDate = o.UpdateDate,
                  }).FirstOrDefault();
                  var str = JsonConvert.SerializeObject(data);
                  result.Data = str;

                  result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
                  return result;
              }
        
             public static ServiceResult<object> Save(ParameterBag input)
             {
                 var result = new ServiceResult<object>();
                 var param = input.Data as string;
                 var jsonParam = (ShopProfile)null;
                 jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

                 var db = input.Context.TSCSEntities;

                 var errors = db.GetEntityValidationErrors(jsonParam);

                 if (errors.IsValid)
                 {
                     var sProfile = db.ShopProfile.FirstOrDefault();
                     if (sProfile != null)
                     {
                         sProfile.ShopName = jsonParam.ShopName;
                         sProfile.Phone1 = jsonParam.Phone1;
                         sProfile.Phone2 = jsonParam.Phone2;
                         sProfile.Address = jsonParam.Address;
                         sProfile.Email = jsonParam.Email;
                         sProfile.Website = jsonParam.Website;
                         sProfile.FacebookLink = jsonParam.FacebookLink;
                         sProfile.StartInvoiceId = jsonParam.StartInvoiceId;
                         sProfile.LogoDirectory = jsonParam.LogoDirectory;
                         
                         sProfile.UpdateDate = DateTime.Now;
                     }
                     else
                     {
                         jsonParam.CreatedDate = DateTime.Now;
                         
                         db.ShopProfile.Add(jsonParam);
                     }
                     db.SaveChanges();
                     result.Message = "   سەرکەوتوبویت";
                     result.SetType(ServiceResultType.SUCCESS);
                 }
                 else
                 {
                     result.ExtraData.Add("ValidationErrors", errors.ValidationErrors);
                     result.SetType(ServiceResultType.FAIL);
                 }


                 return result;
             }
            

    }

}
