﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class StudentHelper
    {
     
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, StudentId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            var db = input.Context.SSAMSEntities;

            var query = db.Student.AsQueryable();
            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.StudentName.Contains(jsonParam.SearchText));

            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText1))
                query = query.Where(o => o.ClassDetail.Class.ClassName.Contains(jsonParam.SearchText1));

            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText2))
                query = query.Where(o => o.ClassDetail.RoomName.Contains(jsonParam.SearchText2));

            var allCount = query.Count();

            Expression<Func<Student, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.StudentName,
                    o.ClassDetail.ClassId,
                    o.ClassDetail.Class.ClassName,
                    o.ClassDetail.RoomName,
                    o.Surname,
                    o.Phone1,
                    o.Phone2,
                    o.MotherName,
                    o.Address,
                    o.Description,
                    o.ClassDetailId
                };
            else
                obj = o => new
                {
                    o.Id,
                    o.StudentName,
                    o.ClassDetail.ClassId,
                    o.ClassDetail.Class.ClassName,
                    o.ClassDetail.RoomName,
                    o.Surname,
                    o.Phone1,
                    o.Phone2,
                    o.MotherName,
                    o.Address,
                    o.Description,
                    o.ClassDetailId

                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj, allCount));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
    


        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Student.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.StudentName,
                o.ClassDetail.ClassId,
                o.ClassDetail.Class.ClassName,
                o.ClassDetail.RoomName,
                o.Surname,
                o.Phone1,
                o.Phone2,
                o.MotherName,
                o.Address,
                o.Description,
                o.ClassDetailId

            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }



        public static ServiceResult<object> GetStudentByClassId(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0};
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Student.Where(o => o.ClassDetailId == jsonParam.Id).Select(o => new
            {
                o.Id,
                StudentId = o.Id,
                o.StudentName,
                o.ClassDetail.ClassId,
                o.ClassDetail.Class.ClassName,
                o.ClassDetail.RoomName,
                o.Surname,
                o.Phone1,
                o.Phone2,
                o.MotherName,
                o.Address,
                o.Description,
                o.ClassDetailId

            }).ToList();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (Student)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var item = db.Student.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {

                item.StudentName = jsonParam.StudentName;
                item.Surname = jsonParam.Surname;
                item.Phone1 = jsonParam.Phone1;
                item.Phone2 = jsonParam.Phone2;
                item.MotherName = jsonParam.MotherName;
                item.Description = jsonParam.Description;
                item.Address = jsonParam.Address;
                item.ClassDetailId = jsonParam.ClassDetailId;
                item.UpdateDate = DateTime.Now;
            }
            else
            {
                jsonParam.UpdateDate = DateTime.Now;
                jsonParam.CreatedDate = DateTime.Now;
                db.Student.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            db.Student.Remove(db.Student.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "Successfull";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
