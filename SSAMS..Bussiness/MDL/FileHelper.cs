﻿
using ACAD.TOOL;
using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Bussiness.MDL
{
    public class FileHelper
    {
        public static ServiceResult<object> FileUpload(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (FileTool)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var fileName = "";

            if (string.IsNullOrEmpty(jsonParam.Content))
            {
                jsonParam.HttpPostedFile = (System.Web.HttpPostedFileWrapper)input.Context["HttpPostedFile"];
                fileName = jsonParam.UploadFile2();
            }
            else
                fileName = jsonParam.UploadFile();


            result.Data = JsonConvert.SerializeObject(new { FileName = fileName });
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
    }
}
