﻿using Newtonsoft.Json;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using SSAMS.DAL;

namespace SSAMS.Bussiness.MDL
{
    public class GroupDetailHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
            var jsonParam2 = new { IsDetail = false, ClassId = (int?)0 };
            jsonParam2 = JsonConvert.DeserializeAnonymousType(param, jsonParam2);

            jsonParam.Order.Col = "Id";
            var db = input.Context.SSAMSEntities;

            var query = db.GroupDetail.AsQueryable();

            if (jsonParam != null && !string.IsNullOrEmpty(jsonParam.SearchText))
                query = query.Where(o => o.ClassName.Contains(jsonParam.SearchText));

            Expression<Func<GroupDetail, object>> obj = null;
            if (jsonParam2.IsDetail)
                obj = o => new
                {
                    o.Id,
                    o.GroupId,
                    o.ClassName,
                    o.StudentId,
                    o.Status

                };
            else
                obj = o => new
                {
                    o.Id,
                    o.GroupId,
                    o.ClassName,
                    o.StudentId,
                    o.Status

                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

       
        public static ServiceResult<object> GetById(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var query = db.Group.Where(o => o.Id == jsonParam.Id).Select(o => new
            {
                o.Id,
                o.GroupName
            }).SingleOrDefault();

            result.Data = JsonConvert.SerializeObject(query);
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> EditGroupDetail(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (GroupDetail)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;

            var item = db.GroupDetail.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.Status = jsonParam.Status;
            }
            else
            {

                result.Message = "Not Found...";
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (GroupDetail)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            var errors = db.GetEntityValidationErrors(jsonParam);

            var item = db.GroupDetail.Where(o => o.Id == jsonParam.Id).SingleOrDefault();
            if (item != null)
            {
                item.GroupId = jsonParam.GroupId;
                item.ClassName = jsonParam.ClassName;
                item.StudentId = jsonParam.StudentId;
            }
            else
            {

                db.GroupDetail.Add(jsonParam);
            }

            db.SaveChanges();

            result.Message = "Successfull...";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.SSAMSEntities;
            db.GroupDetail.Remove(db.GroupDetail.Single(o => o.Id == jsonParam.Id));

            db.SaveChanges();

            result.Message = "Successfull";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }

     

    }
}
