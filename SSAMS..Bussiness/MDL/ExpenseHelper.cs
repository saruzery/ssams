﻿using Newtonsoft.Json;
using SSAMS.DAL;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tigem.Business;
using System.Data.Entity.Core.Objects;
using Tigem.Tools;
using Tigem.Business.Common;

namespace SSAMS.Bussiness.MDL
{
    public class ExpenseHelper
    {
        public static ServiceResult<object> GetList(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = (WebDataTableFilterView)null;
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);
      
            var db = input.Context.TSCSEntities;

            var query = db.Expense.AsQueryable();
          
            Expression<Func<Expense, object>> obj = null;
                obj = o => new
                {
                    o.Id,
                    o.User.Name,
                    o.User.Surname,
                    o.CreatedDate,
                    o.UpdateDate,
                    TotalAmount = (decimal?)o.ExpenseItems.Sum(t => t.TotalAmount),
                    ExpenseDetail = o.ExpenseItems.Select(t => new
                    {
                        t.Id,
                        t.ExpenseId,
                        t.Product,
                        t.Description,
                        t.Quantity,
                        t.UnitPrice,
                        t.TotalAmount
                    }).ToList()
                };
            var str = JsonConvert.SerializeObject(WebDataTableListNew.Create(query, jsonParam, obj));
            result.Data = str;
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }


        public static ServiceResult<object> Save(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Expense = (Expense)null, ExpenseItems = GenericUtilityFunctions.CastList((ExpenseItem)null) };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var expense = jsonParam.Expense;
            var expenseList = jsonParam.ExpenseItems;

            var db = input.Context.TSCSEntities;
            var errors = db.GetEntityValidationErrors(expense);

            if (errors.IsValid)
            {

                foreach (var exp in expenseList)
                {
                    errors = db.GetEntityValidationErrors(exp);
                    if (!errors.IsValid)
                        break;
                }
                if (!expenseList.Any())
                {
                    errors.ValidationErrors.Add(new System.Data.Entity.Validation.DbValidationError("No Item", "There is no item!"));
                }

                if (errors.ValidationErrors.Count() > 0)
                {
                    result.ExtraData.Add("ValidationErrors", errors.ValidationErrors);
                    result.SetType(ServiceResultType.FAIL);
                    return result;
                }
                var item = db.Expense.Where(o => o.Id == expense.Id).SingleOrDefault();
                if (item != null)
                {

                    var newIdList = expenseList.Where(o => o.Id != 0).Select(o => o.Id).Distinct().ToArray();
                    var oldIdList = item.ExpenseItems.Select(o => o.Id).Distinct().ToArray();
                    var removableList = oldIdList.Except(newIdList);
                    foreach (var rm in removableList)
                    {
                        db.ExpenseItem.Remove(item.ExpenseItems.FirstOrDefault(t => t.Id == rm));
                    }
                    foreach (var exId in expenseList.Where(o => o.Id != 0))
                    {
                        var exIt = item.ExpenseItems.First(x => x.Id == exId.Id);
                        exIt.Product = exId.Product;
                        exIt.Description = exId.Description;
                        exIt.Quantity = exId.Quantity;
                        exIt.UnitPrice = exId.UnitPrice;
                        exIt.TotalAmount = exId.TotalAmount;
                    }
                    foreach (var exIt in expenseList.Where(o => o.Id == 0))
                    {
                        item.ExpenseItems.Add(exIt);
                    }
                    item.UpdateDate = DateTime.Now;
                }
                else if (expense.Id == 0)
                {
                    expense.CreatedDate = DateTime.Now;
                    expense.UpdateDate = DateTime.Now;
                    foreach (var exIt in expenseList)
                    {
                        expense.ExpenseItems.Add(exIt);
                    }

                    db.Expense.Add(expense);
                }
                else
                {
                    result.ExtraData.Add("NotFound", "NotFound");
                    result.SetType(ServiceResultType.FAIL);
                    return result;
                }

                db.SaveChanges();
                result.Data = jsonParam.Expense.Id;
                result.Message = "  سەرکەوتوبویت";
                result.SetType(ServiceResultType.SUCCESS);

            }
            else
            {
                result.ExtraData.Add("ValidationErrors", errors.ValidationErrors);
                result.SetType(ServiceResultType.FAIL);
            }

            return result;
        }

        public static ServiceResult<object> GetById(ParameterBag input)
        {
            return GenericHelper<Expense>.GetById(input, o => new
            {
                o.Id,
                o.User.Name,
                o.User.Surname,
                o.CreatedDate,
                o.UpdateDate,
                TotalAmount = (decimal?)o.ExpenseItems.Sum(t => t.TotalAmount),
                Expenses = o.ExpenseItems.Select(t => new
                {
                    t.Product,
                    t.Description,
                    t.Quantity,
                    t.UnitPrice,
                    t.Discount,
                    t.TotalAmount
                }).ToList(),
            });
        }

        public static ServiceResult<object> Remove(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var jsonParam = new { Id = 0 };
            jsonParam = JsonConvert.DeserializeAnonymousType(param, jsonParam);

            var db = input.Context.TSCSEntities;


            db.ExpenseItem.RemoveRange(db.ExpenseItem.Where(o => o.ExpenseId == jsonParam.Id));
            db.Expense.Remove(db.Expense.Single(o => o.Id == jsonParam.Id));
            db.SaveChanges();

            result.Message = "سەرکەوتوبویت";
            result.SetType(ServiceResultType.SUCCESS);
            return result;
        }
    }
}
