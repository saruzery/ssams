﻿using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Tigem.Tools;
using SSAMS.Tools;
using SSAMS.Tools;
using SSAMS.DAL;

namespace Tigem.Business.Common
{
    public class GenericHelper<T> where T : class,IAcadEntity, new()
    {
        public static ServiceResult<object> GetById(ParameterBag input, Expression<Func<T, object>> type)
        {
            var json = input.Data as string;
            var paramType = new { Id = 0 };
            paramType = JsonConvert.DeserializeAnonymousType(json, paramType);
            var db = input.Context.SSAMSEntities;
            var item = db.Set<T>().Where(o => o.Id == paramType.Id).Select(type).SingleOrDefault();
            var result = new ServiceResult<object>
            {
                Data = JsonConvert.SerializeObject(item),
            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> GetList(ParameterBag input, Expression<Func<T, object>> type, Expression<Func<T, bool>> expFilter = null, Expression<Func<T, object>> expOrder = null, int? take = null)
        {

            //var paramType = new { Id = 0 };
            var json = input.Data as string;
            //paramType = JsonConvert.DeserializeAnonymousType(json, paramType);
            var db = input.Context.SSAMSEntities;
            var list = db.Set<T>().AsQueryable();
            if (expFilter != null)
            {
                list = list.Where(expFilter);
            }
            if (expOrder != null)
            {
                list = list.OrderBy(expOrder);
            }
            if (take.HasValue)
                list = list.Take(take.Value);
            var resList = list.Select(type).ToList();
            var result = new ServiceResult<object>
            {
                Data = JsonConvert.SerializeObject(resList),
            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> Save(ParameterBag input)
        {
            //var paramType = new { Id = 0 };            
            var json = input.Data as string;
            var paramType = JsonConvert.DeserializeObject<T>(json);
            var db = input.Context.SSAMSEntities;
            db.Set<T>().AddOrUpdate(paramType);
            db.SaveChanges();
            var result = new ServiceResult<object>
            {
                Data = paramType.Id,
                Message = "Başarılı Şekilde Kaydedildi"

            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        public static ServiceResult<object> Delete(ParameterBag input)
        {
            var paramType = new { Id = 0 };
            var json = input.Data as string;
            paramType = JsonConvert.DeserializeAnonymousType(json, paramType);
            //var paramType = JsonConvert.DeserializeObject<T>(json);
            var db = input.Context.SSAMSEntities;
            db.Database.Log = o => Debug.WriteLine(o);
            var entity = new T { Id = paramType.Id };
            db.Set<T>().Attach(entity);
            db.Set<T>().Remove(entity);
            db.SaveChanges();
            var result = new ServiceResult<object>
            {
                Data = paramType.Id
            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
    }
}

//FORIGN KEY CONSTRAINT CHECK
//try
//   {
//       db.SaveChanges();
//   }
//   catch (DbUpdateException ex)
//   {
//       var sqlException = ex.GetBaseException() as SqlException;

//if (sqlException != null)
//{
//    var number = sqlException.Number;

//    if (number == 547)
//    {
//Regex.Match(sqlException.Message, "(?<=table \")[a-zA-Z.]*").Groups[0].Value;
//        Console.WriteLine("Must delete products before deleting category");
//    }
//}
//   }

