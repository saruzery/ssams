﻿using SSAMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Bussiness._Common.Utility.Authorization
{
    
    public class CookieData
    {
        public CookieData(int userId, string userName, bool isLock, RoleEnum role)
        {
            UserId = userId;
            UserName = userName;
            IsLock = isLock;
            Role = role;
        }
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsLock { get; set; }
        public RoleEnum Role { get; set; }
        
    }
}
