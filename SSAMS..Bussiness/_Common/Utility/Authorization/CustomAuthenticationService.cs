﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace SSAMS.Bussiness._Common.Utility.Authorization
{
    public class CustomAuthenticationService
    {
        public static void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public static void SignIn(CookieData data, bool rememberMe = false)
        {
            //var ser = new XMLSerializer();
            //var userData = ser.Serialize(data);
            SetCookie(JsonConvert.SerializeObject(data), data.UserName.ToString(), rememberMe);
        }

        public static void Lock()
        {
            var identity = (FormsIdentity)HttpContext.Current.User.Identity;
            var data = JsonConvert.DeserializeObject<CookieData>(identity.Ticket.UserData);
            data.IsLock = true;
            SetCookie(JsonConvert.SerializeObject(data), data.UserName, identity.Ticket.IsPersistent);
        }
        public static void LockOut()
        {
            var identity = (FormsIdentity)HttpContext.Current.User.Identity;
            var data = JsonConvert.DeserializeObject<CookieData>(identity.Ticket.UserData);
            data.IsLock = false;
            SetCookie(JsonConvert.SerializeObject(data), data.UserName, identity.Ticket.IsPersistent);
        }
        public static CookieData GetAuthenticationCookieObject()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var identity = (FormsIdentity)HttpContext.Current.User.Identity;
                var ticket = identity.Ticket;
                //var ser = new XMLSerializer();
                //var data = (CookieData)ser.Deserialize(ticket.UserData, typeof(CookieData));
                var data = JsonConvert.DeserializeObject<CookieData>(ticket.UserData);
                //var data = new CookieData(int.Parse(ticket.UserData), ticket.Name);
                return data;
            }
            return null;
        }

        private static void SetCookie(string userData, string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(2, userName, DateTime.Now, DateTime.Now.AddMinutes(20), isPersistent, userData,
                FormsAuthentication.FormsCookiePath);
            // Encrypt the ticket.
            string encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
             HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
             
        }
    }
}
