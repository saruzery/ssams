﻿using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Tigem.Tools;
using SSAMS.Tools;
using SSAMS.BUSSINESS._Common;

namespace SSAMS.Bussiness.Common
{
    public class EnumHelper
    {
        public static ServiceResult<object> GetEnumList(ParameterBag input)
        {
            var json = input.Data as string;
            var paramType = new { EnumType = "" };
            paramType = JsonConvert.DeserializeAnonymousType(json, paramType);
            var type = typeof(UserInfo).Assembly.GetTypes().SingleOrDefault(o => o.Name == paramType.EnumType);
            var res = EnumExtensions.GetListOfDescription(type);

            var result = new ServiceResult<object>
            {
                Data = JsonConvert.SerializeObject(res),
            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

        public static ServiceResult<object> GetEnumDictionary(ParameterBag input)
        {
            var json = input.Data as string;
            var paramType = new { EnumType = "" };
            paramType = JsonConvert.DeserializeAnonymousType(json, paramType);
            var type = typeof(UserInfo).Assembly.GetTypes().SingleOrDefault(o => o.Name == paramType.EnumType);
            var res = EnumExtensions.GetListOfDescription(type);
            var dict = res.ToDictionary(o => o.Value, o => o);
            var result = new ServiceResult<object>
            {
                Data = JsonConvert.SerializeObject(dict)
            };
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }

    }
}