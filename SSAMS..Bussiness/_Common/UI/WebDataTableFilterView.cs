﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tigem.Business
{
    public class WebDataTableFilterView
    {
        public WebDataTableFilterView()
        {
            ItemCountInPage = 10;
            Order = new WebDataTableOrder();
        }
        public class WebDataTableOrder
        {
            public string Col { get; set; }
            public bool Direction { get; set; }
        }
        public string SearchText { get; set; }

        public string SearchText1 { get; set; }
        public string SearchText2 { get; set; }

        public string SearchCustomerType { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public int? Type { get; set; }
        public WebDataTableOrder Order { get; set; }

        public int ItemCountInPage { get; set; }
    }
}
