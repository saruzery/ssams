﻿using SSAMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using Tigem.Business;

namespace SSAMS.Bussiness
{

    public class WebDataTableList
    {

        public object ItemList { get; set; }
        public int SelectedPage { get; set; }
        public int TotalCount { get; set; }
    }
    public static class WebDataTableListNew
    {
        public static WebDataTableList<T> Create<T>(IQueryable<T> itemList, WebDataTableFilterView paramType, Expression<Func<T, object>> selectExp, int? allcount=null) where T : class, IAcadEntity, new()
        {
            var all = itemList;
            var selectedPage = 1;

            all = (paramType.Order != null && !string.IsNullOrEmpty(paramType.Order.Col)) ? all.OrderBy(paramType.Order.Col + " " + (paramType.Order.Direction ? "" : "descending")) : all.OrderBy(o => o.Id);
            
            if (paramType.PageNumber.HasValue)
            {
                selectedPage = paramType.PageNumber ?? 1;
            }

            var totalCount = all.Count();

            if (paramType.ItemCountInPage>-1)
                all = all.Skip((selectedPage - 1) * paramType.ItemCountInPage).Take(paramType.ItemCountInPage);

            var list = all.Select(selectExp).ToList();

            return new WebDataTableList<T>()
            {
                ItemList = list,
                SelectedPage = selectedPage,
                TotalCount = totalCount,
                AllCount = allcount
            };
        }
    }
    public class WebDataTableList<T>
    {

        public List<object> ItemList { get; set; }
        public int SelectedPage { get; set; }
        public int TotalCount { get; set; }
        public int? AllCount { get; set; }
    }
}