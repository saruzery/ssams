﻿using SSAMS.Bussiness;
using SSAMS.DAL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace SSAMS.BUSSINESS._Common
{
    public class RequestBag
    {
        const string DefaultLanguageCookieVal = "TR";
        private readonly Dictionary<string, object> _parameters;
        public SSAMSEntities SSAMSEntities { get; set; }

        //public TigemSettings TigemSettings { get; set; }
        public UserInfo User { get; set; }
        public object Data { get; set; }

        public RequestBag()
        {
            SSAMSEntities = new SSAMSEntities();
            _parameters = new Dictionary<string, object>();

            HttpContext.Current.Request.Cookies.OfType<HttpCookie>().ToList().ForEach(o =>
            {
                if (_parameters.ContainsKey(o.Name))
                    throw new Exception("ACAD Key already used..");
                _parameters.Add(o.Name, o.Value);
            }
                );
            SSAMSEntities.Database.Log = o => Debug.WriteLine(o);
            //TigemSettings = new TigemSettings();
            //var firstOrDefault = TigemDBEntities.Stores.FirstOrDefault();
            //if (firstOrDefault != null)
            //   User = firstOrDefault.UserInfo;

        }

        public object this[string key]
        {
            get
            {
                return _parameters.ContainsKey(key) ? _parameters[key] : null;
            }
            set
            {
                _parameters[key] = value;
            }
        }

        public HttpCookieCollection Cookies
        {
            get
            {
                return HttpContext.Current.Request.Cookies;
            }
        }

        public HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }

        public string Language
        {
            get
            {
                if (Cookies.AllKeys.Contains("LANG")) return Cookies["LANG"].Value;                
                SetCookie("LANG", DefaultLanguageCookieVal, DateTime.MaxValue);
                return DefaultLanguageCookieVal;
            }
        }
        
        public void SetCookie(string name, string value, DateTime expireDate)
        {
            var cookie = new HttpCookie(name) { Expires = expireDate, Value = value };
            HttpContext.Current.Response.Cookies.Add(cookie);            
        }

    }
}
