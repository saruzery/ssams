﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SSAMS.BUSSINESS._Common;

namespace SSAMS.Tools
{
    public class ParameterBag
    {
        const string DefaultRequestItemVal = "ACAD";
        public RequestBag Context { get; set; }

        public ParameterBag(object parameterValue)
        {
            if (HttpContext.Current.Items.Contains(DefaultRequestItemVal))
                Context = HttpContext.Current.Items[DefaultRequestItemVal] as RequestBag;
            else
            {
                Context = new RequestBag();
                HttpContext.Current.Items[DefaultRequestItemVal] = Context;
            }
            Data = parameterValue;
        }

        public object Data
        {
            get { return Context.Data; }
            private set
            {
                Context.Data = value;
            }
        }
    }
}
