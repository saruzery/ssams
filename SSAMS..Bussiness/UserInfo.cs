﻿using SSAMS.Bussiness._Common.Utility.Authorization;
using SSAMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Bussiness
{
    public class UserInfo : CookieData
    {
         public UserInfo(int userId, string userName, RoleEnum role, bool isLock = false)
            : base(userId, userName, isLock, role)
        {
        }
    }
}
