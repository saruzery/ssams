using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Tigem.Tools
{
    public class EnumView
    {
        public int Value { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
    public static class EnumExtensions
    {
        public static List<EnumView> GetListOfDescription(Type t)
        {

            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => new EnumView
            {
                Description = x.GetDescription(),
                Value = Convert.ToInt32(x),
                Name = x.ToString(),
            }).ToList();
        }

        public static string GetDescription(this Enum value)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            if (name == null) return null;
            var field = type.GetField(name);
            if (field != null)
            {
                var attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attr != null)
                {
                    return attr.Description;
                }
            }
            return null;
        }
    }
}