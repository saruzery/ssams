﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Tigem.Tools
{
    public class GenericUtilityFunctions
    {
        public static string GetName<TSource, TField>(Expression<Func<TSource, TField>> Field)
        {
            return (Field.Body as MemberExpression ?? ((UnaryExpression)Field.Body).Operand as MemberExpression).Member.Name;
        }
        public static T Clone<T>(T obj)
        {

            var newObj = Activator.CreateInstance(typeof(T));
            foreach (var pi in obj.GetType().GetProperties())
            {
                if (pi.CanRead && pi.CanWrite && pi.PropertyType.IsSerializable && pi.PropertyType.IsValueType)
                {
                    pi.SetValue(newObj, pi.GetValue(obj, null), null);
                }
            }
            return (T)newObj;

        }
        public static List<T> CastList<T>(T typeHolder)
        {
            // typeHolder above is just for compiler magic
            // to infer the type to cast x to
            return new List<T>();
        }
    }

}
