﻿using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace SSAMS.TOOL
{
    public class UploadFile
    {
        /*
        public static ServiceResult<object> UploadImage(ParameterBag input)
        {
            var result = new ServiceResult<object>();
            var param = input.Data as string;
            var paramType = new
            {
                Info = new { Name = "" },
                Content = "",
                Location = STT_FileAttachment.FilePathEnum.PurhasingWayBill,
                GUID = ""
            };
            paramType = JsonConvert.DeserializeAnonymousType(param, paramType);

            //var base64Data = Regex.Match(paramType.Content, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var base64Data = paramType.Content.Substring(paramType.Content.IndexOf(",", StringComparison.Ordinal) + 1);
            var binData = Convert.FromBase64String(base64Data);
            var path = "";
            if (paramType.Location == STT_FileAttachment.FilePathEnum.UserDisiplinary)
                path = HttpContext.Current.Server.MapPath(RequestBag.Current.TigemSettings.FilePaths.UploadUserDiciplinary);
            else if (paramType.Location == STT_FileAttachment.FilePathEnum.PurhasingWayBill)
                path = HttpContext.Current.Server.MapPath(RequestBag.Current.TigemSettings.FilePaths.UploadPurchasingWayBill);
            else if (paramType.Location == STT_FileAttachment.FilePathEnum.UserInfo)
                path = HttpContext.Current.Server.MapPath(RequestBag.Current.TigemSettings.FilePaths.UserInfo);

            Directory.CreateDirectory(path);

            var milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            var newFileName = Path.GetFileNameWithoutExtension(paramType.Info.Name) + "-" + milliseconds + Path.GetExtension(paramType.Info.Name);
            var fullFileName = Path.Combine(path, newFileName);

            File.WriteAllBytes(fullFileName, binData);


            var db = new TigemDBEntities();
            var item = new STT_FileAttachment
            {
                FileEnumPath = paramType.Location,
                FileName = newFileName,
                UploadDate = DateTime.Now,
                ObjectId = paramType.GUID
            };
            db.STT_FileAttachments.Add(item);
            db.SaveChanges();

            result.Data = JsonConvert.SerializeObject(new
            {

                item.Id,
                item.UploadDate,
                ServerFileName = newFileName
            });
            result.Message = "Başarılı Şekilde Kaydedildi";
            result.SetType(ServiceResultType.SUCCESS_WITH_DATA);
            return result;
        }
        */
    }
}
