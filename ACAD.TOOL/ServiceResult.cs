﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Tools
{

    public class ServiceResult<T>
    {

        private ServiceResultType _type = ServiceResultType.FAIL;
        
        public string Type { get { return _type.ToString(); } private set { throw new Exception("Do not use this!!!"); } }

        
        public string Message { get; set; }

        
        public string StackTrace { get; set; }

        
        public T Data { get; set; }

        public Dictionary<string, object> ExtraData { get; private set; }

        
        public bool HasError
        {
            get
            {
                return _type == ServiceResultType.FAIL ||
                    _type == ServiceResultType.NOT_VALID ||
                    _type == ServiceResultType.NOT_AUTHENTICATED ||
                    _type == ServiceResultType.NOT_AUTHORIZED;
            }
            private set { throw new Exception("Do not use this!!!"); }
        }

        public void SetType(ServiceResultType type)
        {
            this._type = type;
        }

        public ServiceResult()
        {
            ExtraData = new Dictionary<string, object>();
        }
    }


    public enum ServiceResultType
    {
        SUCCESS, FAIL, NOT_VALID, SUCCESS_WITH_DATA, NOT_AUTHENTICATED, NOT_AUTHORIZED
    }
}
