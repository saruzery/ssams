﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Tigem.Tools;

namespace SSAMS.BUSSINESS.Reporting
{
    /* 
   public static partial class ReportingTool
   {
     public static string GenerateReceptionDemandEstimatedCostPDF(UserArticle item)
       {
           var fileName = string.Format("ReceptionDemandEstimatedCost{0}.pdf", item.Id);
           var folderPath =
               HttpContext.Current.Server.MapPath(RequestBag.Current.TigemSettings.FilePaths.PDFReceptionDemand);
           var fullFilePath = Path.Combine(folderPath, fileName);
           //TODO
           //if (File.Exists(fullFilePath))
           //    return fileName;
           Directory.CreateDirectory(folderPath);
           //  Create a Document object
           Document document = new Document(PageSize.A4, 25, 25, 10, 25);

           //MemoryStream         

           using (MemoryStream myMemoryStream = new MemoryStream())
           {
               var writer = PdfWriter.GetInstance(document, myMemoryStream);
               string arialuniTff = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
                   "ARIALUNI.TTF");
               BaseFont bf4 = BaseFont.CreateFont(arialuniTff, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);


               // First, create our fonts
               var titleFont = new Font(bf4, 14, Font.BOLD);
               var boldTableFont = new Font(bf4, 10, Font.BOLD);
               var bodyFont = new Font(bf4, 10, Font.NORMAL); //FontFactory.GetFont("Arial", 10 , Font.NORMAL);
               var sbodyFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
               Rectangle pageSize = writer.PageSize;

               // Open the Document for writing
               document.Open();
               //Add elements to the document here
               //Image Singature
               var logo =
                   iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/Assets/Images/logo.png"));
               //logo.SetAbsolutePosition(pageSize.GetLeft(300), 100);
               document.Add(logo);

               // Create the header table 
               PdfContentByte cb1 = new PdfContentByte(writer);

               cb1 = writer.DirectContent;
               cb1.BeginText();
               cb1.SetFontAndSize(bf4, 14);
               cb1.SetTextMatrix(pageSize.GetLeft(240), 825);
               cb1.ShowText("TARIM İŞLETMELERİ GENEL MÜDÜRLÜĞÜ  ");
               cb1.EndText();
               PdfContentByte cb2 = new PdfContentByte(writer);

               cb2 = writer.DirectContent;
               cb2.BeginText();
               cb2.SetFontAndSize(bf4, 14);
               cb2.SetTextMatrix(pageSize.GetLeft(240), 810);
               cb2.ShowText(" TARIM İŞLETMESİ MÜDÜRLÜĞÜ ");

               cb2.EndText();

               cb2 = writer.DirectContent;
               cb2.BeginText();
               cb2.SetFontAndSize(bf4, 12);
               cb2.SetTextMatrix(pageSize.GetLeft(400), 780);
               //cb2.ShowText(EnumExtensions.GetDescription(item.Type));

               cb2.EndText();



               #region Top table


               PdfPTable nested = new PdfPTable(2);

               nested.SpacingAfter = 40;




               nested.SetWidths(new float[] { 2, 2 }); // then set the column's __relative__ widths
               nested.HorizontalAlignment = 0;




               nested.DefaultCell.Border = Rectangle.BOX;


               //PdfPCell nextPostCell1 = new PdfPCell(new Phrase("ALICININ ADI ", boldTableFont));
               //nextPostCell1.Rowspan = 3;
               //nextPostCell1.HorizontalAlignment = 0;
               //nextPostCell1.Border = Rectangle.BOX;
               //nested.AddCell(nextPostCell1);

               //PdfPCell nextPostCell2 = new PdfPCell(new Phrase("NAKLİYECİNİN ADI", boldTableFont));
               //nextPostCell2.Rowspan = 3;
               //nextPostCell2.Border = Rectangle.BOX;
               //nextPostCell2.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell2);


               //PdfPCell nextPostCell5 = new PdfPCell(new Phrase(item.CheckingAccount == null ? "" : item.CheckingAccount.Description, bodyFont));
               //nextPostCell5.Rowspan = 3;
               //nextPostCell5.Border = Rectangle.BOX;
               //nextPostCell5.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell5);

               //PdfPCell nextPostCell6 = new PdfPCell(new Phrase(item.CheckingAccount == null ? "" : item.CheckingAccount.Address, bodyFont));
               //nextPostCell6.Rowspan = 3;
               //nextPostCell6.Border = Rectangle.BOX;
               //nextPostCell6.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell6);

               //PdfPCell nextPostCell3 = new PdfPCell(new Phrase("GÖNDERİLME EMRİ", boldTableFont));



               //nextPostCell3.Border = Rectangle.BOX;
               //nextPostCell3.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell3);

               //PdfPCell nextPostCell4 = new PdfPCell(new Phrase("Sürücü Bilgileri", boldTableFont));
               //nextPostCell4.Border = Rectangle.BOX;
               //nextPostCell4.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell4);

               //PdfPCell nextPostCell7 = new PdfPCell(new Phrase(item.CheckingAccount == null ? "" : item.CheckingAccount.Town == null ? "" : item.CheckingAccount.Town.Name + "/" + item.CheckingAccount.Town.City.Name, bodyFont));
               //nextPostCell7.Rowspan = 3;
               //nextPostCell7.Border = Rectangle.BOX;
               //nextPostCell7.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell7);

               //PdfPCell nextPostCell8 = new PdfPCell(new Phrase(" "));
               //nextPostCell8.Rowspan = 3;
               //nextPostCell8.Border = Rectangle.BOX;
               //nextPostCell8.HorizontalAlignment = 0;
               //nested.AddCell(nextPostCell8);




               document.Add(nested);

               #endregion

               #region Items Table

               //Create body table
               PdfPTable itemTable = new PdfPTable(4);
               itemTable.HorizontalAlignment = 0;
               itemTable.WidthPercentage = 100;
               itemTable.SetWidths(new float[] { 10, 10, 20, 30 }); // then set the column's __relative__ widths
               itemTable.SpacingAfter = 10;
               itemTable.DefaultCell.Border = Rectangle.BOX;
               PdfPCell cell1 = new PdfPCell(new Phrase("NO", boldTableFont));
               cell1.HorizontalAlignment = 0;
               itemTable.AddCell(cell1);
               PdfPCell cell2 = new PdfPCell(new Phrase("STOK ADI", boldTableFont));
               cell2.HorizontalAlignment = 0;
               itemTable.AddCell(cell2);
               PdfPCell cell3 = new PdfPCell(new Phrase("MİKTARI", boldTableFont));
               cell3.HorizontalAlignment = 0;
               itemTable.AddCell(cell3);
               PdfPCell cell4 = new PdfPCell(new Phrase("FİYATI(TL)", boldTableFont));
               cell4.HorizontalAlignment = 0;
               itemTable.AddCell(cell4);

               foreach (var row in item.TradeDemandDetails)
               {
                   PdfPCell numberCell = new PdfPCell(new Phrase(row.EquipmentType.Code.ToString(), sbodyFont));
                   numberCell.HorizontalAlignment = 0;
                   numberCell.PaddingLeft = 3f;
                   numberCell.PaddingBottom = 3f;
                   numberCell.Border = Rectangle.BOX;

                   itemTable.AddCell(numberCell);

                   PdfPCell descCell = new PdfPCell(new Phrase(row.EquipmentType.Description, bodyFont));
                   descCell.HorizontalAlignment = 0;
                   descCell.PaddingLeft = 3f;
                   descCell.PaddingBottom = 3f;
                   descCell.Border = Rectangle.BOX;

                   itemTable.AddCell(descCell);

                   PdfPCell qtyCell =
                       new PdfPCell(new Phrase(row.Amount + row.EquipmentTypeUnit.UnitSetOfUnit.Unit.Description,
                           bodyFont));
                   qtyCell.HorizontalAlignment = 0;
                   qtyCell.PaddingLeft = 3f;
                   qtyCell.PaddingBottom = 3f;
                   qtyCell.Border = Rectangle.BOX;

                   itemTable.AddCell(qtyCell);

                   PdfPCell amtCell =
                       new PdfPCell(
                           new Phrase(
                               (row.Amount * (row.ApproximatePrice) * row.EquipmentTypeUnit.ConversionFactor).ToString(
                                   "0.00"), bodyFont));
                   amtCell.HorizontalAlignment = 0;
                   amtCell.PaddingLeft = 3f;
                   amtCell.PaddingBottom = 3f;



                   amtCell.Border = Rectangle.BOX;
                   itemTable.AddCell(amtCell);

               }
               // Table footer
               PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(""));
               totalAmtCell1.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
               itemTable.AddCell(totalAmtCell1);
               PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(""));
               totalAmtCell2.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
               itemTable.AddCell(totalAmtCell2);
               PdfPCell totalAmtStrCell = new PdfPCell(new Phrase("TOPLAM FİYAT", boldTableFont));
               totalAmtStrCell.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
               totalAmtStrCell.HorizontalAlignment = 1;
               itemTable.AddCell(totalAmtStrCell);
               PdfPCell totalAmtCell =
                   new PdfPCell(
                       new Phrase(
                           item.TradeDemandDetails.Sum(
                               o => o.Amount * (o.ApproximatePrice) * o.EquipmentTypeUnit.ConversionFactor)
                               .ToString("#,###.00"), boldTableFont));


               totalAmtCell.HorizontalAlignment = 1;
               itemTable.AddCell(totalAmtCell);

               PdfPCell cell = new PdfPCell(new Phrase("*** Para Birimi  TL ***", bodyFont));
               cell.Colspan = 4;
               cell.HorizontalAlignment = 1;
               itemTable.AddCell(cell);
               document.Add(itemTable);

               #endregion

               Chunk transferBank = new Chunk("Banka Hesabınız:", boldTableFont);
               transferBank.SetUnderline(0.1f, -2f); //0.1 thick, -2 y-location
               document.Add(transferBank);
               document.Add(Chunk.NEWLINE);

               // Bank Account Info
               PdfPTable bottomTable = new PdfPTable(3);
               bottomTable.HorizontalAlignment = 0;
               bottomTable.TotalWidth = 300f;
               bottomTable.SetWidths(new int[] { 90, 10, 200 });
               bottomTable.LockedWidth = true;
               bottomTable.SpacingBefore = 20;
               bottomTable.DefaultCell.Border = Rectangle.NO_BORDER;
               bottomTable.AddCell(new Phrase("Hesab No", bodyFont));
               bottomTable.AddCell(":");
               bottomTable.AddCell(new Phrase("hesab no", bodyFont));
               bottomTable.AddCell(new Phrase("Hesab  Adı", bodyFont));
               bottomTable.AddCell(":");
               bottomTable.AddCell(new Phrase("hesabadı", bodyFont));
               bottomTable.AddCell(new Phrase("Şube", bodyFont));
               bottomTable.AddCell(":");
               bottomTable.AddCell(new Phrase("şube", bodyFont));
               bottomTable.AddCell(new Phrase("Banka", bodyFont));
               bottomTable.AddCell(":");
               bottomTable.AddCell(new Phrase("banka", bodyFont));
               document.Add(bottomTable);

               //Approved by
               PdfContentByte cb = new PdfContentByte(writer);
               BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
               cb = writer.DirectContent;
               cb.BeginText();
               cb.SetFontAndSize(bf, 10);
               cb.SetTextMatrix(pageSize.GetLeft(300), 200);
               cb.ShowText("");
               cb.EndText();






               cb = new PdfContentByte(writer);
               bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
               cb = writer.DirectContent;
               cb.BeginText();
               cb.SetFontAndSize(bf, 8);
               cb.SetTextMatrix(pageSize.GetLeft(60), 100);
               cb.ShowText(
                   "Siparişiniz için Teşekkürederiz! Siparişiniz hakkında her hanği bir probleminiz olursa lütfen bizimle Tigem.com üzerinden irtibata geçiniz.");

               cb.EndText();

               writer.CloseStream = false; //set the closestream property
               // Close the Document without closing the underlying stream
               document.Close();

               byte[] content = myMemoryStream.ToArray();
               var file2Name = string.Format("report{0}.pdf", Guid.NewGuid());

               // Write out PDF from memory stream.
               using (var fs = new FileStream(fullFilePath, FileMode.Create))
               {
                   fs.Write(content, 0, (int)content.Length);
               }


               return fileName;
           }

       }

   }
     * */
}