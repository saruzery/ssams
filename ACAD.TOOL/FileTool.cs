﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace ACAD.TOOL
{
    public enum FileLocation:byte{
        Temp=0,
        Advertisement=1,
        Slider=2,
        CKEditor=4,
        Menu=8,
        Carousel=16
    }

    public enum FileSize : int
    {
        F_Org = 0,
        F_200x150 = 1,
        F_150x45 = 2,
        F_200x200 = 4,
        F_600x315 = 8,
        F_750x350 = 16,
        F_1120x630 = 32,
        F_1200x630 = 64,
        F_1162x432 = 128,
        F_400x210 = 256,

    }

    

    public class FileTool
    {
        public Info Info { get; set; }
        public string Content { get; set; }
        public FileLocation Location { get; set; }
        public HttpPostedFileWrapper HttpPostedFile { get; set; }

        public string UploadFile()
        {
            
            //var base64Data = Regex.Match(paramType.Content, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            var base64Data = Content.Substring(Content.IndexOf(",", StringComparison.Ordinal) + 1);
            var binData = Convert.FromBase64String(base64Data);
            var path = FindFileLocation();
           
            Directory.CreateDirectory(path);

            var newFileName = Guid.NewGuid() + Path.GetExtension(Info.Name);
            var fullFileName = Path.Combine(path, newFileName);

            File.WriteAllBytes(fullFileName, binData);

            return newFileName;
        }


        public string UploadFile2() {

            string ImageName = HttpPostedFile.FileName;

            var path = FindFileLocation();
            
            Directory.CreateDirectory(path);

            var newFileName = Guid.NewGuid() + Path.GetExtension(ImageName);
            var fullFileName = Path.Combine(path, newFileName);

            HttpPostedFile.SaveAs(fullFileName);

            //if (Location == FileLocation.Advertisement)
                MakeThumbnailImage(newFileName);

            return newFileName;
        }

        private string FindFileLocation()
        {
            var str = "";
            if (Location == FileLocation.Advertisement)
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Advertisement");
            else if (Location == FileLocation.Slider)
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Slider");
            else if (Location == FileLocation.CKEditor)
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/CKEditor");
            else if (Location == FileLocation.Menu)
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Menu");
            else if (Location == FileLocation.Carousel)
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Carousel");
            else
                str = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Temp");

            return HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Logo");
        }

        private void MakeThumbnailImage(string imageName)
        {
            //var fileFullPath = @"C:\Users\ufuko\Pictures\WP2T4GYC.jpg";
            var path = FindFileLocation();
            var orgImagePath = Path.Combine(path, imageName);

            var list = new List<Size> 
	{
		new Size {Width=200,Height=150},
		new Size {Width=150,Height=45},
		new Size {Width=200,Height=200},
		new Size {Width=600,Height=315},
		new Size {Width=750,Height=350},
		new Size {Width=560,Height=315},
		new Size {Width=280,Height=158},
		
		new Size {Width=1162,Height=432},
        new Size {Width=400,Height=210},
	};
            foreach (var e in list)
            {
                var newFileName = string.Format("{0}-{1}X{2}{3}", Path.GetFileNameWithoutExtension(orgImagePath), e.Width, e.Height, Path.GetExtension(orgImagePath));
                var newFileFullPath = Path.Combine(path, newFileName);
                Resize(orgImagePath, newFileFullPath, e,  Path.GetExtension(Info.Name));
            }
            //list.ForEach(o=>Resize(fileFullPath,Path.GetFullPath))
        }
        private static void Resize(string imageFile, string outputFile, Size sz, string frmt)
        {
            using (var srcImage = Image.FromFile(imageFile))
            {
                int sourceWidth = srcImage.Width;
                int sourceHeight = srcImage.Height;
                int sourceX = 0;
                int sourceY = 0;
                int destX = 0;
                int destY = 0;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                nPercentW = ((float)sz.Width / (float)sourceWidth);
                nPercentH = ((float)sz.Height / (float)sourceHeight);
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                    destX = System.Convert.ToInt16((sz.Width -
                                  (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = System.Convert.ToInt16((sz.Height -
                                  (sourceHeight * nPercent)) / 2);
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                using (var newImage = new Bitmap(sz.Width, sz.Height))
                {
                    newImage.SetResolution(srcImage.HorizontalResolution,
                             srcImage.VerticalResolution);
                    using (var graphics = Graphics.FromImage(newImage))
                    {
                        //graphics.Clear(Color.Red);
                        graphics.SmoothingMode = SmoothingMode.AntiAlias;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        graphics.DrawImage(srcImage, new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);
                        if (frmt != ".png")
                            newImage.Save(outputFile, System.Drawing.Imaging.ImageFormat.Jpeg);
                        else
                            newImage.Save(outputFile, System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
                    
                
            }
        }
        public static void Resize(string imageFile, string outputFile, FileSize size, string frmt)
        {
            Size sz = new Size { Width = 100, Height = 50 };
            if (size == FileSize.F_200x150)
                sz = new Size {Width=200,Height=150};
            else if (size == FileSize.F_150x45)
                sz = new Size {Width=150,Height=45};
            else if (size == FileSize.F_200x200)
                sz = new Size {Width=200,Height=200};
            else if (size == FileSize.F_600x315)
                sz = new Size {Width=600,Height=315};
            else if (size == FileSize.F_750x350)
                sz = new Size {Width=750,Height=350};
            else if (size == FileSize.F_1120x630)
                sz = new Size {Width=1120,Height=630};
            else if (size == FileSize.F_1200x630)
                sz = new Size {Width=1200,Height=630};
            else if (size == FileSize.F_1162x432)
                sz =new Size {Width=1162,Height=432};
            else if (size == FileSize.F_400x210)
                sz = new Size { Width = 400, Height = 210 };

            Resize(imageFile, outputFile, sz, frmt);
        }
    }

    public class Info
    {
        public string Name { get; set; }
    }
}
