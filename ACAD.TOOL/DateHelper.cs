﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAMS.Tools
{
    public class DateHelper
    {
        public static DateTime? ParseTurkishDate(String date)
        {
            DateTime? result = null;

            try
            {
                result = DateTime.ParseExact(date, "dd.MM.yyyy",
                                             System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static DateTime? ParseSqlDate(String date)
        {
            DateTime? result = null;

            try
            {
                result = DateTime.ParseExact(date, "yyyyMMdd",
                                             System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static DateTime? ParseSqlDateTime(String date)
        {
            DateTime? result = null;

            try
            {
                result = DateTime.ParseExact(date, "yyyyMMddhhmmss",
                                             System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static String formatTurkishDate(DateTime? date)
        {
            String result = "";

            if (date != null && date.HasValue && !date.Value.Equals(new DateTime()))
            {
                result = date.Value.ToString("dd.MM.yyyy");
            }

            return result;
        }

        public static String FormatTurkishDateColumn(Object o)
        {
            String result = "";

            if (o != null && o != DBNull.Value)
            {
                DateTime date = Convert.ToDateTime(o);
                result = date.ToString("dd.MM.yyyy");
            }

            return result;
        }


        public static String formatSqlDate(DateTime? date)
        {
            String result = "";

            if (date.HasValue && !date.Value.Equals(new DateTime()))
            {
                result = date.Value.ToString("yyyyMMdd");
            }

            return result;
        }


        public static String formatSqlDateTime(DateTime? date)
        {
            String result = "";

            if (date.HasValue && !date.Value.Equals(new DateTime()))
            {
                result = date.Value.ToString("yyyyMMddhhmmss");
            }

            return result;
        }



    }

    public static partial class DateTimeExtensions
    {
        public static DateTime FirstDayOfWeek(this DateTime dt)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var diff = dt.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
            if (diff < 0)
                diff += 7;
            return dt.AddDays(-diff).Date;
        }

        public static DateTime LastDayOfWeek(this DateTime dt)
        {
            return dt.FirstDayOfWeek().AddDays(6);
        }

        public static DateTime FirstDayOfMonth(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public static DateTime LastDayOfMonth(this DateTime dt)
        {
            return dt.FirstDayOfMonth().AddMonths(1).AddDays(-1);
        }

        public static DateTime FirstDayOfNextMonth(this DateTime dt)
        {
            return dt.FirstDayOfMonth().AddMonths(1);
        }
    }
}
