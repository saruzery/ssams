namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public enum RoleEnum : byte
    {
        Teacher = 1,
        Manager = 2,
        Admin = 32
    }

    [Table("AUTH_User")]
    public partial class User : IAcadEntity
    {
        public User()
        {
            UserActivities = new HashSet<UserActivity>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(2000)]
        public string Name { get; set; }

        [Required]
        [StringLength(2000)]
        public string Surname { get; set; }

        [Required]
        [StringLength(2000)]
        [Index("IX_User_Email", IsUnique = true)]
        public string Email { get; set; }

        [Required]
        [StringLength(2000)]
        public string Password { get; set; }

        public RoleEnum Role { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public virtual ICollection<UserActivity> UserActivities { get; set; }

        public virtual ICollection<TeacherAssignment> TeacherAssignment { get; set; }

    }
}
