namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AUTH_UserActivity")]
    public partial class UserActivity : IAcadEntity
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(2000)]
        public string BrowserName { get; set; }

        [Required]
        [StringLength(2000)]
        public string IPAddress { get; set; }

        [Required]
        [StringLength(2000)]
        public string BrowserPlatform { get; set; }

        public DateTime VisitDate { get; set; }


        [ForeignKey("UserId")]
        public virtual User User { get; set; }


    }
}
