﻿using System.Data.Entity.ModelConfiguration.Conventions;

namespace SSAMS.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SSAMSEntities : DbContext
    {
        public SSAMSEntities()
            : base("name=SSAMSEntities")
        {
        }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserActivity> UserActivities { get; set; }
        public virtual DbSet<Class> Class { get; set; }
        public virtual DbSet<ClassDetail> ClassDetail { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupDetail> GroupDetail { get; set; }
        public virtual DbSet<Lecture> Lecture { get; set; }
        public virtual DbSet<Student> Student { get; set; }

        public virtual DbSet<TeacherAssignment> TeacherAssignment { get; set; }
       
        public System.Data.Entity.Validation.DbEntityValidationResult GetEntityValidationErrors(object entity)
        {
            return this.Entry(entity).GetValidationResult();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

           

        }
    }
}



