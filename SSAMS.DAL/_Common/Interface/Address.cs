﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace SSAMS.DAL._Common.Interface
{
    [ComplexType]
    public class Address
    {
        public string Street { get; set; }        
        public int TownId { get; set; }
        public string ZipCode { get; set; }
    }
}
