﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SSAMS.DAL._Common.Interface
{
    public interface IAcadCode
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        string Code { get; set; }

        [Required]
        int CodePart1 { get; set; }

        int? CodePart2 { get; set; }

        int? CodePart3 { get; set; }

        int? CodePart4 { get; set; }

        int? CodePart5 { get; set; }

        void SetCodeWithParts();
    }
}