﻿using System.ComponentModel.DataAnnotations;

namespace SSAMS.DAL._Common.Interface
{
    public interface IPerson
    {
        [Required]
        [StringLength(500)]
        string Name { get; set; }

        [Required]
        [StringLength(500)]
        string Surname { get; set; }
    }
}