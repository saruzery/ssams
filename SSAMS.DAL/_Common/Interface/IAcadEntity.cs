﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SSAMS.DAL
{
    public interface IAcadEntity
    {
        [Key]
        int Id { get; set; }
    }
}
