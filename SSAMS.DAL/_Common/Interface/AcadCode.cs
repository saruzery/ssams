﻿using System;
using System.Linq;

namespace SSAMS.DAL._Common.Interface
{
    public class AcadCode
    {
        public const int ConstPadLength = 5;
        public const int ConstGroupPadLength = 3;
        public static void SetCodeWithParts(IAcadCode entity)
        {
            var codeparts = entity.Code.Trim().Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (codeparts.Count < 5)
                codeparts.AddRange(Enumerable.Range(1, 5 - codeparts.Count).Select(o => ""));
            entity.CodePart1 = int.Parse(codeparts[0]);
            entity.CodePart2 = string.IsNullOrEmpty(codeparts[1]) ? null : (int?)int.Parse(codeparts[1]);
            entity.CodePart3 = string.IsNullOrEmpty(codeparts[2]) ? null : (int?)int.Parse(codeparts[2]);
            entity.CodePart4 = string.IsNullOrEmpty(codeparts[3]) ? null : (int?)int.Parse(codeparts[3]);
            entity.CodePart5 = string.IsNullOrEmpty(codeparts[4]) ? null : (int?)int.Parse(codeparts[4]);
            entity.Code = string.Format("{0}{1}{2}{3}{4}",
                entity.CodePart1,
                entity.CodePart2.HasValue ? "." + entity.CodePart2.Value.ToString().PadLeft(AcadCode.ConstGroupPadLength, '0') : "",
                entity.CodePart3.HasValue ? "." + entity.CodePart3.Value.ToString().PadLeft(AcadCode.ConstGroupPadLength, '0') : "",
                entity.CodePart4.HasValue ? "." + entity.CodePart4.Value.ToString().PadLeft(AcadCode.ConstGroupPadLength, '0') : "",
                entity.CodePart5.HasValue ? "." + entity.CodePart5.Value.ToString().PadLeft(AcadCode.ConstGroupPadLength, '0') : "");
        }
    }
}