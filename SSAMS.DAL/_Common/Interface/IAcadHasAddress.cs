﻿namespace SSAMS.DAL._Common.Interface
{
    public interface IAcadHasAddress
    {
        Address Address { get; set; }
    }
}