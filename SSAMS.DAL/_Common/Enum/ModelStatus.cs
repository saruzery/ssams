﻿using System;
using System.ComponentModel;

namespace SSAMS.DAL._Common.Enum
{
    [Flags]
    public enum ModelStatus : byte
    {
        [Description("Pasif")]
        Passive = 0,
        [Description("Aktif")]
        Active = 2
    }
}