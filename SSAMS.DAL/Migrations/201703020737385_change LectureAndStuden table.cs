namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLectureAndStudentable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Student", new[] { "ClassId" });
            RenameColumn(table: "dbo.Student", name: "ClassId", newName: "Class_Id");
            AddColumn("dbo.Lecture", "Description", c => c.String());
            AddColumn("dbo.Student", "ClassDetailId", c => c.Int(nullable: false));
            AlterColumn("dbo.Student", "Class_Id", c => c.Int());
            CreateIndex("dbo.Student", "ClassDetailId");
            CreateIndex("dbo.Student", "Class_Id");
            AddForeignKey("dbo.Student", "ClassDetailId", "dbo.ClassDetail", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Student", "ClassDetailId", "dbo.ClassDetail");
            DropIndex("dbo.Student", new[] { "Class_Id" });
            DropIndex("dbo.Student", new[] { "ClassDetailId" });
            AlterColumn("dbo.Student", "Class_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Student", "ClassDetailId");
            DropColumn("dbo.Lecture", "Description");
            RenameColumn(table: "dbo.Student", name: "Class_Id", newName: "ClassId");
            CreateIndex("dbo.Student", "ClassId");
        }
    }
}
