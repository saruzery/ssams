namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Class",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lecture",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassId = c.Int(nullable: false),
                        LectureName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Class", t => t.ClassId)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(),
                        LectureId = c.Int(),
                        GroupName = c.String(),
                        Description = c.String(),
                        StartDateTime = c.DateTime(),
                        EndDateTime = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lecture", t => t.LectureId)
                .ForeignKey("dbo.AUTH_User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.LectureId);
            
            CreateTable(
                "dbo.GroupDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(),
                        StudentId = c.Int(),
                        ClassName = c.String(),
                        Status = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Group", t => t.GroupId)
                .ForeignKey("dbo.Student", t => t.StudentId)
                .Index(t => t.GroupId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentName = c.String(),
                        Surname = c.String(),
                        Phone1 = c.String(),
                        Phone2 = c.String(),
                        MotherName = c.String(),
                        Address = c.String(),
                        Description = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        ClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Class", t => t.ClassId)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.TeacherAssignment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClassId = c.Int(nullable: false),
                        GroupId = c.Int(nullable: false),
                        LectureId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Class", t => t.ClassId)
                .ForeignKey("dbo.Group", t => t.GroupId)
                .ForeignKey("dbo.Lecture", t => t.LectureId)
                .ForeignKey("dbo.AUTH_User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ClassId)
                .Index(t => t.GroupId)
                .Index(t => t.LectureId);
            
            CreateTable(
                "dbo.AUTH_User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 2000),
                        Surname = c.String(nullable: false, maxLength: 2000),
                        Email = c.String(nullable: false, maxLength: 2000),
                        Password = c.String(nullable: false, maxLength: 2000),
                        Role = c.Byte(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true, name: "IX_User_Email");
            
            CreateTable(
                "dbo.AUTH_UserActivity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        BrowserName = c.String(nullable: false, maxLength: 2000),
                        IPAddress = c.String(nullable: false, maxLength: 2000),
                        BrowserPlatform = c.String(nullable: false, maxLength: 2000),
                        VisitDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AUTH_User", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Group", "UserId", "dbo.AUTH_User");
            DropForeignKey("dbo.AUTH_UserActivity", "UserId", "dbo.AUTH_User");
            DropForeignKey("dbo.TeacherAssignment", "UserId", "dbo.AUTH_User");
            DropForeignKey("dbo.TeacherAssignment", "LectureId", "dbo.Lecture");
            DropForeignKey("dbo.TeacherAssignment", "GroupId", "dbo.Group");
            DropForeignKey("dbo.TeacherAssignment", "ClassId", "dbo.Class");
            DropForeignKey("dbo.Group", "LectureId", "dbo.Lecture");
            DropForeignKey("dbo.GroupDetail", "StudentId", "dbo.Student");
            DropForeignKey("dbo.Student", "ClassId", "dbo.Class");
            DropForeignKey("dbo.GroupDetail", "GroupId", "dbo.Group");
            DropForeignKey("dbo.Lecture", "ClassId", "dbo.Class");
            DropIndex("dbo.AUTH_UserActivity", new[] { "UserId" });
            DropIndex("dbo.AUTH_User", "IX_User_Email");
            DropIndex("dbo.TeacherAssignment", new[] { "LectureId" });
            DropIndex("dbo.TeacherAssignment", new[] { "GroupId" });
            DropIndex("dbo.TeacherAssignment", new[] { "ClassId" });
            DropIndex("dbo.TeacherAssignment", new[] { "UserId" });
            DropIndex("dbo.Student", new[] { "ClassId" });
            DropIndex("dbo.GroupDetail", new[] { "StudentId" });
            DropIndex("dbo.GroupDetail", new[] { "GroupId" });
            DropIndex("dbo.Group", new[] { "LectureId" });
            DropIndex("dbo.Group", new[] { "UserId" });
            DropIndex("dbo.Lecture", new[] { "ClassId" });
            DropTable("dbo.AUTH_UserActivity");
            DropTable("dbo.AUTH_User");
            DropTable("dbo.TeacherAssignment");
            DropTable("dbo.Student");
            DropTable("dbo.GroupDetail");
            DropTable("dbo.Group");
            DropTable("dbo.Lecture");
            DropTable("dbo.Class");
        }
    }
}
