namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateinclassdetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClassDetail", "RoomName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClassDetail", "RoomName");
        }
    }
}
