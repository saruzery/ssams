namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addroomName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GroupDetail", "RoomName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GroupDetail", "RoomName");
        }
    }
}
