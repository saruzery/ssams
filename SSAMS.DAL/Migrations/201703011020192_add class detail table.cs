namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addclassdetailtable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClassDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassId = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Class", t => t.ClassId)
                .Index(t => t.ClassId);
            
            AddColumn("dbo.Class", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassDetail", "ClassId", "dbo.Class");
            DropIndex("dbo.ClassDetail", new[] { "ClassId" });
            DropColumn("dbo.Class", "Description");
            DropTable("dbo.ClassDetail");
        }
    }
}
