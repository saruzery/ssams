﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Web;

namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.SqlClient;
    using System.Linq;

    public sealed partial class Configuration : DbMigrationsConfiguration<SSAMSEntities>
    {
        public Configuration()
        {
            this.MigrationsDirectory = "Migrations";
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(SSAMSEntities db)
        {
            // GenerateData(db);

        }

        public static void GenerateData(SSAMSEntities db)
        {

            if (!db.Users.Any())
            {
                CreateSQLScripts(new SSAMSEntities(), "~/App_Data/Sample/User.sql");
            }
        }

        private static void CreateSQLScripts(SSAMSEntities db, params string[] filePaths)
        {
            var con = new SqlConnection(db.Database.Connection.ConnectionString);
            var cmd = con.CreateCommand();
            cmd.CommandTimeout = int.MaxValue;
            for (int i = 0; i < filePaths.Count(); i++)
            {
                con.Open();
                var lines = File.ReadAllText(HttpContext.Current.Server.MapPath(filePaths[i]));

                cmd.CommandText = lines.Replace("GO", ";").Replace("go ", ";");
                cmd.ExecuteNonQuery();
                cmd.CommandText = "";
                con.Close();
            }
        }
    }
}
