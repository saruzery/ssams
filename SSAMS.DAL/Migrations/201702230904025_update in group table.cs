namespace SSAMS.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateingrouptable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Group", "StartHour", c => c.Int(nullable: false));
            AddColumn("dbo.Group", "StartMinute", c => c.Int(nullable: false));
            AddColumn("dbo.Group", "EndHour", c => c.Int(nullable: false));
            AddColumn("dbo.Group", "EndMinute", c => c.Int(nullable: false));
            AlterColumn("dbo.Group", "StartDateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Group", "EndDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Group", "EndDateTime", c => c.DateTime());
            AlterColumn("dbo.Group", "StartDateTime", c => c.DateTime());
            DropColumn("dbo.Group", "EndMinute");
            DropColumn("dbo.Group", "EndHour");
            DropColumn("dbo.Group", "StartMinute");
            DropColumn("dbo.Group", "StartHour");
        }
    }
}
