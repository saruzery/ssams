﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Class")]
    public partial class Class : IAcadEntity
    {
          public Class()
        {
            ClassDetail = new HashSet<ClassDetail>();
        }
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        
        public virtual ICollection<Lecture> Lecture { get; set; }

        public virtual ICollection<Student> Student { get; set; }

        public virtual ICollection<TeacherAssignment> TeacherAssignment { get; set; }
        public virtual ICollection<ClassDetail> ClassDetail { get; set; }
    }
}
