﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lecture")]
    public partial class Lecture : IAcadEntity
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public string LectureName { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        [ForeignKey("ClassId")]
        public virtual Class Class { get; set; }
        public virtual ICollection<TeacherAssignment> TeacherAssignment { get; set; }
        public virtual ICollection<Group> Group { get; set; }


    }
}
