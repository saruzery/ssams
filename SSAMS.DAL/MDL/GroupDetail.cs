﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public enum Status : int
    {
        Empty = 0,
        Attend=1,
        Absence = 2,
        Permission = 4,
    }

    [Table("GroupDetail")]
    public partial class GroupDetail : IAcadEntity
    {
        public int Id { get; set; }
        public int? GroupId { get; set; }
        public int? StudentId { get; set; }
        public String ClassName { get; set; }

        public String RoomName { get; set; }

        public Status? Status { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }

        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }

    }
}
