﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Group")]
    public partial class Group : IAcadEntity
    {
        public Group()
        {
            GroupDetail = new HashSet<GroupDetail>();
            TeacherAssignment = new HashSet<TeacherAssignment>();
        }
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? LectureId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public DateTime StartDateTime { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public DateTime EndDateTime { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("LectureId")]
        public virtual Lecture Lecture { get; set; }


        public virtual ICollection<GroupDetail> GroupDetail { get; set; }

        public virtual ICollection<TeacherAssignment> TeacherAssignment { get; set; }

    }
}
