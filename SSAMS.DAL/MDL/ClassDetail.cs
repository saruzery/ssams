﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClassDetail")]
    public partial class ClassDetail : IAcadEntity
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public string RoomName { get; set; }
        public string Description { get; set; }
        [ForeignKey("ClassId")]
        public virtual Class Class { get; set; }
    }
}
