﻿namespace SSAMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student : IAcadEntity
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public string Surname { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MotherName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public int ClassDetailId { get; set; }

        [ForeignKey("ClassDetailId")]
        public virtual ClassDetail ClassDetail { get; set; }

        public virtual ICollection<GroupDetail> GroupDetail { get; set; }


    }
}
