﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using SSAMS.WEB.Filters;
using SSAMS.WEB.Models;
using SSAMS.Bussiness._Common.Utility.Authorization;
using SSAMS.Tools;
using SSAMS.Bussiness.USR;
using Newtonsoft.Json;
using System.IO;

namespace SSAMS.WEB.Controllers
{
    //[Authorize]
   // [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        public ActionResult Index()
        {
            var cookie = CustomAuthenticationService.GetAuthenticationCookieObject();
            if (cookie != null)
                if (cookie.IsLock)
                    return RedirectToAction("Lock");
                else
                    return RedirectToAction("Index", "Home");
            /*else
                if (Request.IsLocal)
                {
                var item = new { Email = "admin", Pass = "123Admin456" };
                    UserHelper.SignIn(new ParameterBag(JsonConvert.SerializeObject(item)));
                    return RedirectToAction("Index", "Home");
                }*/
            return View();
        }
        //
        // GET: /Account/Login


        public ActionResult Login()
        {
            return RedirectToAction("Index");
        }
        [HttpPost]
        [AllowAnonymous]
        public string SignIn(string Email, string Pass)
        {

            var parms = new StreamReader(this.Request.InputStream).ReadToEnd();
            var datatype = new { parms = "" };
            var item = JsonConvert.DeserializeAnonymousType(parms, datatype);
            var input = new ParameterBag(item.parms);

            return JsonConvert.SerializeObject(UserHelper.SignIn(input));
        }

        public ActionResult Logout()
        {
            CustomAuthenticationService.SignOut();
            return RedirectToAction("Index");
        }
    }
}
