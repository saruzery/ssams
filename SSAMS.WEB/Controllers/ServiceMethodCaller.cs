﻿using Newtonsoft.Json;
using SSAMS.Bussiness;
using SSAMS.Bussiness._Common.Utility.Authorization;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SSAMS.WEB.Controllers
{
    public class ServiceMethodCaller
    {
        private static Type[] helperMedhods = typeof(UserInfo).Assembly.GetTypes().Where(o => o.Name.EndsWith("Helper")).ToArray();

        public delegate ServiceResult<T> ServiceMethod<T>(ParameterBag input);

        public static ServiceResult<T> CallServiceMethod<T>(ServiceMethod<T> method, ParameterBag param)
        {

            ServiceResult<T> result = new ServiceResult<T>();
            result.SetType(ServiceResultType.FAIL);
            result.Message = "Servis tamamlanamadı. " + method.GetType() + "." + method.Method.Name;
            /*
            if (!SessionHelper.isLoggedIn()) //! ünlem işareti test amaçlı kaldırıldı
            {
                result.SetType(ServiceResultType.NOT_AUTHENTICATED);
                result.Message = "Kullanıcı girişi yapılmamış.";
            }
            else
            {
             * */
            if (param == null)
            {
                param = new ParameterBag(null);
            }
            var user = CustomAuthenticationService.GetAuthenticationCookieObject() as CookieData;

            if (param.Context.User == null && user != null)
            {
                param.Context.User = new UserInfo(user.UserId, user.UserName, user.Role, user.IsLock);//GetAuthenticationCookieObject(); new UserInfo(RequestBag.Current.User.Id, RequestBag.Current.User.Name, RequestBag.Current.User.Surname);
            }
            else
            {
                result.SetType(ServiceResultType.NOT_AUTHENTICATED);
                return result;
            }
            //param.User = SessionHelper.getSessionData();
            //Globals.GetDefinedBaseUrl();

            //param.User.UserId = RequestBag.Current.User.Id;
            try
            {
                result = method(param);
            }
            catch (Exception ex)
            {
                result.SetType(ServiceResultType.FAIL);
                result.Message = ex.Message;
                result.StackTrace = ex.StackTrace;
            }
            /*

                            //try
                            //{
                            //    if (param.Data != null || result.Data == null || result.Type.Equals(ServiceResultType.FAIL.ToString()))
                            //    {
                            //        String inputStr = "";
                            //        if (param.Data is string)
                            //        {
                            //            inputStr = param.Data as string;
                            //        }
                            //        else
                            //        {
                            //            inputStr = vs.CustomSerialize(param.Data); //notification save içinde exception verdiğinden normal serialize düzeltildikten sonra customserialize kullanılacak
                            //        }
                            //        int id = result.ExtraData.Keys.Contains("SAVED_ID") ? Convert.ToInt32(result.ExtraData["SAVED_ID"]) : 0;
                            //        String tableName = result.ExtraData.Keys.Contains("SAVED_TABLENAME") ? result.ExtraData["SAVED_TABLENAME"].ToString() : "";
                            //        string className = method.Method.DeclaringType.Name;
                            //        string methodName = method.Method.Name;
                            //        string resultstr = result.Type.ToString();
                            //        String outputStr = "";

                            //        if (result.Type.Equals(ServiceResultType.FAIL.ToString()) ||
                            //            result.Type.Equals(ServiceResultType.NOT_AUTHORIZED.ToString()))
                            //        {

                            //            outputStr += "\t" + result.Message + "\t" + result.StackTrace;
                            //        }
                            //        try
                            //        {
                            //            string ip = HttpContext.Current.Request.UserHostAddress;
                            //            string browser = HttpContext.Current.Request.Browser.Browser;
                            //            string platform = HttpContext.Current.Request.Browser.Platform;

                            //            LOG_Helper.logService(SmartWebGlobals.GetConnectionString(), SmartSessionHelper.getUserId(), className, methodName, tableName, id, inputStr, resultstr, outputStr, ip, browser, platform);
                            //        }
                            //        catch (Exception)
                            //        {
                            //            LOG_Helper.logService(SmartWebGlobals.GetConnectionString(), SmartSessionHelper.getUserId(), className, methodName, tableName, id, inputStr, resultstr, outputStr, "", "", "");
                            //        }
                            //    }
                            //}
                            //catch (Exception)
                            //{
                            //}

                        }*/

            result.ExtraData.Clear();

            return result;
        }

        public static ServiceResult<T> CallServiceMethod<T>(ParameterBag param)
        {
            var result = new ServiceResult<T>();
            result.SetType(ServiceResultType.FAIL);
            var fullName = HttpContext.Current.Request.QueryString["Method"];

            if (param == null)
            {
                param = new ParameterBag(null);
            }
            var user = CustomAuthenticationService.GetAuthenticationCookieObject() as CookieData;

            if (param.Context.User == null && user != null)
            {
                param.Context.User = new UserInfo(user.UserId, user.UserName, user.Role, user.IsLock);//GetAuthenticationCookieObject(); new UserInfo(RequestBag.Current.User.Id, RequestBag.Current.User.Name, RequestBag.Current.User.Surname);
            }

            var methodName = fullName.Substring(fullName.IndexOf(".", StringComparison.Ordinal) + 1);
            /*if (AutorizationRequred.Any(t => t.Trim() == methodName.Trim()) && param.Context.User == null)
            {
                result.SetType(ServiceResultType.NOT_AUTHENTICATED);
                return result;
            }*/

            //param.User.UserId = RequestBag.Current.User.Id;
            try
            {
                var methodObj = GetMethodInfo<T>(fullName);
                var action = (ServiceMethod<T>)Delegate.CreateDelegate
                              (typeof(ServiceMethod<T>), methodObj);
                result = action(param);
            }
            catch (Exception ex)
            {
                if (result == null)
                    result = new ServiceResult<T>();
                HandleException(ex, result);
            }

            result.ExtraData.Clear();
            return result;
        }
        // public static List<string> AutorizationRequred = new String[] { "GetList", "GetById", "Save" }.ToList();
        private static MethodInfo GetMethodInfo<T>(string fullName)
        {
            MethodInfo methodObj;
            var className = "";
            var methodName = "";
            try
            {
                className = fullName.Substring(0, fullName.IndexOf(".", StringComparison.Ordinal)) + "Helper";
                methodName = fullName.Substring(fullName.IndexOf(".", StringComparison.Ordinal) + 1);

                //TODO CACHE Yaz yoksa yavaş çalışır                
                var res = helperMedhods.SingleOrDefault(o => o.Name == className);

                if (res == null || (methodObj = res.GetMethod(methodName)) == null)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Helper-Class Name typo:{0}/{1}", className, methodName));
            }
            return methodObj;
        }

        public static void HandleException<T>(Exception ex, ServiceResult<T> result)
        {
            result.SetType(ServiceResultType.FAIL);

            var baseEx = ex.GetBaseException();
            /*if (baseEx is DbEntityValidationException)
            {
                var dbEx = baseEx as DbEntityValidationException;
                result.Message =
                 JsonConvert.SerializeObject(dbEx.EntityValidationErrors.SelectMany(o => o.ValidationErrors).Select(o => new { o.PropertyName, o.ErrorMessage }));
            }
            else if (baseEx is SqlException)
            {
                var sqlEx = baseEx as SqlException;
                if (sqlEx.Errors.Count > 0)
                {
                    //if(sqlEx.Errors[0].Number==547)
                }
                result.Message = JsonConvert.SerializeObject(sqlEx.Message);
            }
            else
            {*/
            result.Message = baseEx.Message;
            //}


            var st = new StackTrace(ex, true); // create the stack trace
            var query = st.GetFrames().Select(frame => new
            {
                FileName = frame.GetFileName(),
                LineNumber = frame.GetFileLineNumber(),
                ColumnNumber = frame.GetFileColumnNumber(),
                Method = frame.GetMethod(),
                Class = frame.GetMethod().DeclaringType,
            }).Where(o => o.FileName != null).Select(o => new
            {
                FileName = (MakeRelativePath(HttpContext.Current.Server.MapPath("~/"), o.FileName)),
                o.LineNumber,
                o.ColumnNumber,
                o.Method,
                o.Class
            }).ToList();
            result.StackTrace = JsonConvert.SerializeObject(query);
            try
            {
                SavePersist(JsonConvert.SerializeObject(result));
            }
            catch (Exception)
            {

            }



        }

        private static void SavePersist(string result)
        {
            var folderPath = HttpContext.Current.Server.MapPath("~/Uploads/");
            var filePath = Path.Combine(folderPath, "Error.log");
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
            File.AppendAllLines(filePath, new List<string> { "------------------", DateTime.Now.ToString(), result });
            if (HttpContext.Current.Request.IsLocal)
                return;
            /*new Thread(() =>
            {
                SmtpEmail.SendEmail(new[] { System.Web.Configuration.WebConfigurationManager.AppSettings["Acad_ErrorMail"]
            }, "MARS", result, new string[] { });
                
            }).Start();
            */
        }

        public static string MakeRelativePath(string fromPath, string toPath)
        {
            if (string.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (string.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.ToUpperInvariant() == "FILE")
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

    }
}