﻿using ACAD.TOOL;
using Newtonsoft.Json;
using SSAMS.Common;
using SSAMS.DAL;
using SSAMS.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSAMS.WEB.Controllers
{
    [CustomAuthorize(RoleEnum.Teacher)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            var name = "Angular/Index";
            var result = "~/Views/" + name + ".cshtml";
            return PartialView(result);
        }

        [AllowAnonymous]
        public ActionResult GenericView(string name)
        {
            var result = "~/Views/" + name + ".cshtml";
            return PartialView(result);
            //return View();
        }

        public string CallHelper()
        {
            var finalstate = "";
            string parameter = null;
            try
            {
                var parms = new StreamReader(this.Request.InputStream).ReadToEnd();
                parameter = parms;
                finalstate = "parms";
                var datatype = new { parms = "" };
                var item = JsonConvert.DeserializeAnonymousType(parms, datatype);
                finalstate = "jsonconvert";
                var input = new ParameterBag(item.parms);
                finalstate = "parameterbag";

                return JsonConvert.SerializeObject(ServiceMethodCaller.CallServiceMethod<object>(input));
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { finalstate = finalstate, parms = parameter });
            }

        }

        [OutputCache(Duration = 864000)]
        public FileResult GetImageFile(string orgFileUrl, FileSize size)
        {
            var path = Server.MapPath("~/App_Data/Uploads");
            var orgName = Path.GetFileNameWithoutExtension(Path.Combine(path, orgFileUrl));
            var extension = Path.GetExtension(Path.Combine(path, orgFileUrl));
            var fileSize = "";
            if (size == FileSize.F_200x150)
                fileSize = String.Format("-{0}X{1}", 200, 150);
            else if (size == FileSize.F_150x45)
                fileSize = String.Format("-{0}X{1}", 150, 45);
            else if (size == FileSize.F_200x200)
                fileSize = String.Format("-{0}X{1}", 200, 200);


            var originalName = orgFileUrl.Split('/').Last();
            var spl = orgFileUrl.Split('/').Reverse().Skip(1).Reverse().ToArray();
            var s = "";
            if (spl.Length > 0)
            {
                s = string.Join("/", spl);
                path = Path.Combine(path, s);
            }

            var fileName = String.Format("{0}{1}{2}", orgName, fileSize, extension);
            /*
            if (location == FileLocation.Advertisement)
                path = Server.MapPath("~/App_Data/Uploads/Advertisement");
            else if (location == FileLocation.Slider)
            {
                var spl = orgFileUrl.Split('/').Reverse().Skip(1).Reverse().ToArray();

                //.Reverse().Skip(1).Join();
                if (spl.Length > 0)
                {
                    var s = string.Join("/", spl);
                    path = Server.MapPath("~/App_Data/Uploads/");
                }
                else
                    path = Server.MapPath("~/App_Data/Uploads/Slider");
            }
            else if (location == FileLocation.CKEditor)
                path = Server.MapPath("~/App_Data/Uploads/CKEditor");
            else
                path = Server.MapPath("~/App_Data/Uploads/Temp");

            var orgName = Path.GetFileNameWithoutExtension(Path.Combine(path, orgFileUrl));
            var extension = Path.GetExtension(Path.Combine(path, orgFileUrl));
            var fileSize = "";
            if (size == FileSize.F_100x50)
                fileSize = String.Format("-{0}X{1}", 100, 50);
            else if (size == FileSize.F_280x158)
                fileSize = String.Format("-{0}X{1}", 280, 158);
            else if (size == FileSize.F_560x315)
                fileSize = String.Format("-{0}X{1}", 560, 315);
            else if (size == FileSize.F_600x315)
                fileSize = String.Format("-{0}X{1}", 600, 315);
            else if (size == FileSize.F_750x350)
                fileSize = String.Format("-{0}X{1}", 750, 350);
            else if (size == FileSize.F_1120x630)
                fileSize = String.Format("-{0}X{1}", 1120, 630);
            else if (size == FileSize.F_1200x630)
                fileSize = String.Format("-{0}X{1}", 1200, 630);
            else if (size == FileSize.F_1162x432)
                fileSize = String.Format("-{0}X{1}", 1162, 432);
            else if (size == FileSize.F_400x210)
                fileSize = String.Format("-{0}X{1}", 400, 210);


            var fileName = String.Format("{0}{1}{2}", orgName, fileSize, extension);*/
            //TODO QRCodePath
            if (string.IsNullOrEmpty(fileName) || !System.IO.File.Exists(Path.Combine(path, fileName)))
                if (System.IO.File.Exists(Path.Combine(path, originalName)))
                {

                    FileTool.Resize(Path.Combine(path, originalName), Path.Combine(path, fileName), size, extension);
                }
            /*else
            {
                var globalPath = Server.MapPath("~/App_Data/Uploads/Global");
                if (!System.IO.File.Exists(Path.Combine(globalPath, "no-image" + fileSize + ".png")))
                    FileTool.Resize(Path.Combine(globalPath, "no-image.png"), Path.Combine(globalPath, "no-image" + fileSize + ".png"), size, "image/png");
                return File(Path.Combine(globalPath, "no-image" + fileSize + ".png"), "image/png");

            }*/

            return File(Path.Combine(path, fileName), "image/jpeg");
        }

    }
}
