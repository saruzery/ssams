﻿using SSAMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSAMS.Bussiness._Common.Utility.Authorization;

namespace SSAMS.Common
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly RoleEnum? _role;

        public CustomAuthorizeAttribute()
        {
        }
        public CustomAuthorizeAttribute(RoleEnum role)
        {
            _role = role;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var identity = httpContext.User.Identity;
            if (!identity.IsAuthenticated)
            {
                /*if (!HttpContext.Current.Request.IsLocal) return false;
                var dbUserProfile =
                    RequestBag.Current.Context.Users.SingleOrDefault(o => o.Email == "developer.admin@zirve.edu.tr");
                CustomAuthenticationService.SignIn(new Forms.Authorization.CookieData(dbUserProfile.Id,
                    dbUserProfile.Name + " " + dbUserProfile.Surname));*/
                return false;
            }
            var user = CustomAuthenticationService.GetAuthenticationCookieObject();
            if (_role.HasValue)
            {
                if (user.Role < _role.Value)
                    return false;
            }
            return true;
        }
    }
}