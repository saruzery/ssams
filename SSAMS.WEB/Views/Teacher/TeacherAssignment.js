﻿angular.module('NWEGARApp').controller('TeacherAssignmentCtrl', function ($scope, NGService, $timeout) {

    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.modelClass = { ItemList: [] };
    $scope.filterGroup = { ItemCountInPage: -1 };
    $scope.modelGroup  = { ItemList: [] };
    $scope.filterLecture  = { ItemCountInPage: -1 };
    $scope.modelLecture  = { ItemList: [] };
    $scope.filterUser= { ItemCountInPage: -1 };
    $scope.modelUser = { ItemList: [] };

    $scope.filter = { ItemCountInPage: 10 };
    $scope.model = { TeacherAssignmentList: [] };

    $scope.TeacherAssignment = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 2)
            $scope.filter.FromDate = null;


        else if (type == 3)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Remove = function (id) {
        NGService.CallService("TeacherAssignment.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.Cancel = function () {
        $scope.TeacherAssignment = {};
    }
    $scope.Edit = function (TeacherAssignment) {
        $scope.TeacherAssignment = angular.copy(TeacherAssignment);
    }

    $scope.Save = function () {
        NGService.CallService("TeacherAssignment.Save", $scope.TeacherAssignment).then(function (response) {
            $scope.GetList();
            $scope.TeacherAssignment = {};
        }, function (response) {

        });
    };

    $scope.GetClass  = function () {
        NGService.CallService("Class.GetList", $scope.filterClass ).then(function (response) {
            $scope.modelClass  = response.Data;
        }, function (response) {
        });
    };
    $scope.GetGroup  = function () {
        NGService.CallService("Group.GetList", $scope.filterGroup ).then(function (response) {
            $scope.modelGroup  = response.Data;
        }, function (response) {
        });
    };
    $scope.GetLecture  = function () {
        NGService.CallService("Lecture.GetList", $scope.filterLecture ).then(function (response) {
            $scope.modelLecture  = response.Data;
        }, function (response) {
        });
    };
    $scope.GetUser = function () {
        NGService.CallService("User.GetList", $scope.filterUser).then(function (response) {
            $scope.modelUser = response.Data;
        }, function (response) {
        });
    };
    $scope.GetList = function () {
        NGService.CallService("TeacherAssignment.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetList();
    $scope.GetClass ();
    $scope.GetGroup ();
    $scope.GetLecture ();
    $scope.GetUser();

});