﻿angular.module('NWEGARApp').controller('LectureCtrl', function ($scope, NGService, $timeout) {
    var LectureOriginal = {};

    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.modelClass = { ItemList: [] };

    $scope.filter = { ItemCountInPage: 10 };
    $scope.model = { LectureList: [] };

    $scope.Lecture = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 2)
            $scope.filter.FromDate = null;


        else if (type == 3)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Remove = function (id) {
        NGService.CallService("Lecture.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.Cancel = function () {
        $scope.Lecture = {};
    }
    $scope.Edit = function (Lecture) {
        $scope.Lecture = angular.copy(Lecture);
    }

    $scope.Save = function () {
        NGService.CallService("Lecture.Save", $scope.Lecture).then(function (response) {
            $scope.GetList();
            $scope.Lecture = {};
        }, function (response) {

        });
    };

    $scope.GetClass = function () {
        NGService.CallService("Class.GetList", $scope.filterClass).then(function (response) {
            $scope.modelClass = response.Data;
        }, function (response) {
        });
    };

    $scope.GetList = function () {
        NGService.CallService("Lecture.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetList();
    $scope.GetClass();

});