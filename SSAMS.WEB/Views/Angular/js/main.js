﻿/***
Metronic AngularJS App Main Script
***/

/* Metronic App */
var NWEGARApp = angular.module("NWEGARApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
NWEGARApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/
/**
`$controller` will no longer look for controllers on `window`.
The old behavior of looking on `window` for controllers was originally intended
for use in examples, demos, and toy apps. We found that allowing global controller
functions encouraged poor practices, so we resolved to disable this behavior by
default.

To migrate, register your controllers with modules rather than exposing them
as globals:

Before:

```javascript
function MyController() {
  // ...
}
```

After:

```javascript
angular.module('myApp', []).controller('MyController', [function() {
  // ...
}]);

Although it's not recommended, you can re-enable the old behavior like this:

```javascript
angular.module('myModule').config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
**/

//AngularJS v1.3.x workaround for old style controller declarition in HTML
NWEGARApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

NWEGARApp.factory('toastr', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    function addAlert(alert) {
        alert.Date = new Date();
        toastr.alerts.push(alert);
        /*$timeout(function () {
            toastr.alerts.splice(0, 1);
        }, 10000);*/
    }
    function closeAlert(index) {
        toastr.alerts.splice(index, 1);
    }
    var toastr = {
        alerts: [],
        addAlert: addAlert,
        closeAlert: closeAlert
    };

    $rootScope.toastr = toastr;

    return toastr;
}]);


NWEGARApp.factory('NGService', function ($uibModal, $q, $http, toastr, $timeout) {
    var NGService = {};
    var localUrl = "/Home/CallHelper?Method=";
    var loginUrl = "/Account";
    //var o = {
    //    Url: 'http://mywebsite/folder/file.html',
    //    Data: []
    //};
    NGService.CallService = function (helperMethod, postData, blockName) {
        var tag = 'body';
        if (blockName) {
            tag = blockName;
        }
        App.blockUI({
            target: $(tag), animate: true,
            //overlayColor: 'none'
        });

        var d = $q.defer();
        $http({ method: 'POST', data: { parms: JSON.stringify(postData) }, url: localUrl + helperMethod }).then(function (respons, status) {
            console.log(respons);
            var response = respons.data;
            var tip = response.Type;
            if (response.HasError && response.HasError == true) {
                if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                } else {
                    var alert = {};
                    alert.type = tip == 'SUCCESS' || tip == 'SUCCESS_WITH_DATA' ? 'success' : tip == 'FAIL' ? 'danger' : '';
                    alert.msg = response.Message;
                    response.Data = eval('(' + response.Data + ')');
                    if (response.Message)
                        toastr.addAlert(alert);
                    /*if (alert.type == 'success')
                        toastr.success(response.Message);
                    else
                        //toastr.error(response.Message);*/
                    d.reject(response);
                }
            } else {
                if (tip == 'SUCCESS' || tip == 'SUCCESS_WITH_DATA') {
                    //if (tip == 'SUCCESS')
                    // $rootScope.AddAlert({type:'success', msg:response.Message});
                    response.Data = eval('(' + response.Data + ')');
                    if (response.Message)
                        toastr.addAlert({ type: 'success', msg: response.Message });
                    d.resolve(response);
                } else if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                } else {
                    if (response.Message)
                        toastr.addAlert({ type: 'danger', msg: response.Message });
                    response.Data = eval('(' + response.Data + ')');
                    d.reject(response);
                }
            }

            $timeout(function () {
                App.unblockUI(tag);
            }, 500);
        }, function (response) {
            d.reject(response.data);

            $timeout(function () {
                App.unblockUI(tag);
            }, 500);
        });
        return d.promise;
    };

    NGService.CallFileUploadService = function (helperMethod, postData, progress) {

        var d = $q.defer();
        var d2 = $q.defer();
        var cancel = function (reason) {
            d2.resolve(reason);
            d.reject(reason);
        };
        $http({
            method: 'POST', data: { parms: JSON.stringify(postData) }, url: localUrl + helperMethod, timeout: d2.promise, onProgress: function (event) {
                if (event && progress) {
                    console.log("loaded " + ((event.loaded / event.total) * 100) + "%");
                    progress((event.loaded / event.total) * 100, d);
                }
            }
        }).then(function (respons, status) {
            console.log(respons);
            var response = respons.data;
            var tip = response.Type;
            if (response.HasError && response.HasError == true) {
                if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                } else {
                    var alert = {};
                    alert.type = tip == 'SUCCESS' ? 'success' : tip == 'FAIL' ? 'error' : '';
                    alert.msg = response.Message;
                    response.Data = eval('(' + response.Data + ')');
                    if (response.Message)
                        toastr.addAlert(alert);

                    /*if (alert.type == 'success')
                        toastr.success(response.Message);
                    else
                        //toastr.error(response.Message);*/
                    d.reject(response);
                }
            } else {
                if (tip == 'SUCCESS' || tip == 'SUCCESS_WITH_DATA') {
                    //if (tip == 'SUCCESS')
                    // $rootScope.AddAlert({type:'success', msg:response.Message});

                    response.Data = eval('(' + response.Data + ')');
                    if (response.Message)
                        $rootScope.settings.alerts.push({ type: 'success', msg: response.Message });

                    d.resolve(response);
                } else if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                } else {
                    if (response.Message)
                        toastr.addAlert({ type: 'error', msg: response.Message });
                    //toastr.error(response.Message);
                    //$rootScope.AddAlert({ type: 'error', msg: response.Message });
                    response.Data = eval('(' + response.Data + ')');
                    d.reject(response);
                }
            }


        }, function (response) {
            d.reject(response.data);
        });
        return {
            promise: d.promise,
            cancel: cancel
        };
    },


    NGService.GetParameterByName = function (name) {

        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        if (results === null) {
            return location.pathname.substr(location.pathname.lastIndexOf('/') + 3);
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    };

    NGService.OpenModal = function (o) {
        var q = o.Url.split("/");
        var fileName = q[q.length - 1];
        //var folderUrl = q.where(function (o, i) { return i !== q.length - 1; }).join("/");
        //var fullUrl = folderUrl + "/" + fileName;
        //o.Url,o.Data
        return $uibModal.open({
            animation: true,
            templateUrl: 'Home/GenericView?name=' + o.Url,
            controller: fileName + "Ctrl",
            size: o.Size || 'xs',
            backdrop: o.Backdrop || 'static',
            resolve: {
                passData: function () {
                    return o.Data;
                },
                loadModule: ['$ocLazyLoad', '$q', function ($ocLazyLoad, $q) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        files: ['/Views/' + o.Url + ".js"]
                    });
                    //return deferred.promise;
                }]
            }
        });
    }

    NGService.OpenConfirmation = function (o) {
        return $modal.open({
            animation: true,
            templateUrl: "GenericView?name=Common/ConfirmationModal",
            controller: "ConfirmationModalCtrl",
            size: 'md',
            backdrop: 'static',
            resolve: {
                passData: function () {
                    return o;
                },
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'Views/Common/ConfirmationModal.js'
                        ]
                    });
                }]
            }
        });
    },
    NGService.NewGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = crypto.getRandomValues(new Uint8Array(1))[0] % 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    return NGService;
})



/* Setup global settings */
NWEGARApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout',
    };

    $rootScope.settings = settings;

    return settings;
}]);

NWEGARApp.directive('mycounterup', function ($timeout) {
    return {
        template: '{{counter|number:counter!=0?fraction:0}}',
        scope: {
            counter: '=?',
            counterup: '=',
            fraction: '@'
        },
        link: function (scope, element, attrs) {
            scope.counter = 0;
            var maxrange;
            var minrange;
            var isPositive;
            function getRandomArbitrary(min, max) {
                return Math.random() * (max - min) + min;
            }
            function counter(counterUp) {
                if (isPositive === false && scope.counter > counterUp)
                    $timeout(function () { scope.counter -= getRandomArbitrary(minrange, maxrange); counter(counterUp) }, 1);
                else if (isPositive === true && scope.counter < counterUp)
                    $timeout(function () { scope.counter += getRandomArbitrary(minrange, maxrange); counter(counterUp) }, 1);
                else
                    scope.counter = counterUp;
            }
            scope.$watch('counterup', function (newValue, oldValue) {
                if (newValue && newValue != oldValue) {
                    var a = Math.abs(Math.floor(newValue));
                    for (var i = 1; i < 20; i++) {
                        if (Math.pow(10, i) > a) {
                            minrange = i > 2 ? Math.pow(10, i - 3) : 0
                            maxrange = i > 2 ? Math.pow(10, i - 2) : i == 0 ? 1 : i;
                            break;
                        }
                    }
                    isPositive = newValue >= 0;
                    counter(newValue);
                } else {
                    isPositive = true;
                    counter(newValue);
                }
            });
        }
    };
});
NWEGARApp.directive('digitsOnly', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9.]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return parseFloat(transformedInput);
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

NWEGARApp.directive('useraddremove', function ($timeout) {
    return {
        restrict: 'E',
        template: '<div class="input-group"><span class="input-group-btn"><button class="btn default" type="button" ng-click="substract()"><i class="fa fa-minus"></i></button></span><input type="text" disabled class="form-control " style="text-align:center" ng-model="ngModel" /><span class="input-group-btn"><button class="btn default" type="button" ng-click="add()"><i class="fa fa-plus"></i></button></span></div>',
        scope: {
            ngModel: '='
        },
        replace: true,
        require: 'ngModel',
        link: function ($scope, elem, attr, ngModelCtrl) {
            ngModelCtrl.$formatters.push(function (modelValue) {
                modelValue = parseInt(modelValue || 0);

                return modelValue;
            });
            $scope.add = function () {
                $scope.ngModel += 1;
            }
            $scope.substract = function () {
                if ($scope.ngModel > 0) {
                    $scope.ngModel -= 1;
                }
            }
            $scope.$watch('ngModel', function () {
                ngModelCtrl.$setViewValue($scope.ngModel);
            });
            ngModelCtrl.$render = function () {
                if (!ngModelCtrl.$viewValue) ngModelCtrl.$viewValue = 0;

                $scope.ngModel = ngModelCtrl.$viewValue;

            };
        }
    };
});


NWEGARApp.directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);
            element.on('change', function (onChangeEvent) {
                var reader = new FileReader();
                var file = (onChangeEvent.srcElement || onChangeEvent.target).files[0];
                reader.onload = function (onLoadEvent) {
                    scope.$apply(function () {
                        fn(scope, { $file: { Info: file, Content: onLoadEvent.target.result } });
                    });
                };
                reader.readAsDataURL(file);
            });
        }
    };
});

NWEGARApp.controller('ConfirmModalController', function ($scope, $modalInstance, data) {
    $scope.data = angular.copy(data);
    $scope.formData = {};
    $scope.ok = function (isValid, closeMessage) {
        if (isValid && isValid === true)
            $modalInstance.close($scope.formData);
    };

    $scope.cancel = function (dismissMessage) {
        if (angular.isUndefined(dismissMessage)) {
            dismissMessage = 'cancel';
        }
        $modalInstance.dismiss(dismissMessage);
    };

})
  .value('$confirmModalDefaults', {
      template: '<div class="modal-header"><h3 class="modal-title">{{data.title}}</h3></div>' +
      '<div class="modal-body">{{data.text}}</div>' +
      '<div class="modal-footer">' +
      '<button class="btn btn-primary" ng-click="ok(true)">{{data.ok}}</button>' +
      '<button class="btn btn-default" ng-click="cancel()">{{data.cancel}}</button>' +
      '</div>',
      controller: 'ConfirmModalController',
      defaultLabels: {
          title: 'Confirm',
          ok: 'OK',
          cancel: 'نەخێر'
      }
  })
  .factory('$confirm', function ($modal, $confirmModalDefaults) {
      return function (data, settings) {
          var defaults = angular.copy($confirmModalDefaults);
          settings = angular.extend(defaults, (settings || {}));

          data = angular.extend({}, settings.defaultLabels, data || {});

          if ('templateUrl' in settings && 'template' in settings) {
              delete settings.template;
          }

          settings.resolve = {
              data: function () {
                  return data;
              }
          };

          return $modal.open(settings).result;
      };
  })
  .directive('confirm', function ($confirm) {
      return {
          priority: 1,
          restrict: 'A',
          scope: {
              confirmIf: "=",
              ngClick: '&',
              confirm: '@',
              confirmSettings: "=",
              confirmTitle: '@',
              confirmOk: '@',
              confirmCancel: '@'
          },
          link: function (scope, element, attrs) {

              element.unbind("click").bind("click", function ($event) {

                  $event.preventDefault();

                  if (angular.isUndefined(scope.confirmIf) || scope.confirmIf) {

                      var data = { text: scope.confirm };
                      if (scope.confirmTitle) {
                          data.title = scope.confirmTitle;
                      }
                      if (scope.confirmOk) {
                          data.ok = scope.confirmOk;
                      }
                      if (scope.confirmCancel) {
                          data.cancel = scope.confirmCancel;
                      }
                      $confirm(data, scope.confirmSettings || {}).then(scope.ngClick);
                  } else {

                      scope.$apply(scope.ngClick);
                  }
              });

          }
      }
  });


/* Setup App Main Controller */
NWEGARApp.controller('AppController', ['$scope', '$rootScope', 'NGService', function ($scope, $rootScope, NGService) {
    $scope.$on('$viewContentLoaded', function () {
    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
NWEGARApp.controller('HeaderController', ['$scope', '$location', 'NGService', function ($scope, $location, NGService) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initHeader(); // init header
        $scope.checkCookie();
    });
    $scope.LangList = [{ Flag: "us", Text: "English", Syn: "ENG" }, { Flag: "ku", Text: "Kurdish", Syn: "KU" }, { Flag: "ar", Text: "Arabic", Syn: "AR" }];
    $scope.CurrentLanguage = { Flag: "", Text: "", Syn: "" };

    $scope.setCookie = function (flag) {
        for (var i = 0; i < $scope.LangList.length; i++) {
            var lang = $scope.LangList[i];
            if (flag == lang.Flag) {
                $scope.CurrentLanguage = { Flag: lang.Flag, Text: lang.Text, Syn: lang.Syn };
                break;
            }

        }

        var d = new Date();
        d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "LANG" + "=" + $scope.CurrentLanguage.Syn + "; " + expires;

        //$state.reload();
        location.reload();
    }

    $scope.getCookie = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.checkCookie = function () {
        var lang = $scope.getCookie("LANG");
        console.log(lang);
        if (lang === "ENG") {
            $scope.CurrentLanguage = { Flag: "us", Text: "English", Syn: "ENG" };
        } else if (lang === "AR") {
            $scope.CurrentLanguage = { Flag: "ar", Text: "Arabic", Syn: "AR" };
        } else {
            $scope.CurrentLanguage = { Flag: "ku", Text: "Kurdish", Syn: "KU" };
        }

    }
    $scope.User = {};
    NGService.CallService("User.CurrentUser", {}).then(function (response) {
        $scope.User = response.Data;
    }, function (data) {
        console.log(data);
    });
}]);

/* Setup Layout Part - Sidebar */
NWEGARApp.controller('SidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
NWEGARApp.controller('QuickSidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        setTimeout(function () {
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
NWEGARApp.controller('ThemePanelController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
NWEGARApp.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
NWEGARApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/dashboard.html");

    $stateProvider

        // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "/Home/GenericView?name=Home/Dashboard",
            data: { pageTitle: 'Admin Dashboard Template' },
            controller: "DashboardCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/pages/scripts/dashboard.js',
                            '../Views/Home/Dashboard.js',
                        ]
                    });
                }]
            }
        })

            //Manager Home
          .state('ManagerHome', {
              url: "/Home/ManagerHome",
              templateUrl: "/Home/GenericView?name=Home/ManagerHome",
              data: { pageTitle: 'ManagerHome' },
              controller: "ManagerHomeCtrl",
              resolve: {
                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'NWEGARApp',
                          insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                          files: [
                              '../Views/Home/ManagerHome.js',
                          ]
                      });
                  }]
              }
          })
        // ClassSetting
        .state('Class', {
            url: "/Class/Class",
            templateUrl: "/Home/GenericView?name=Class/Class",
            data: { pageTitle: 'Class' },

            controller: "ClassCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Class/Class.js',
                        ]
                    });
                }]
            }
        })
          // Report
        .state('Report', {
            url: "/Report/Report",
            templateUrl: "/Home/GenericView?name=Report/Report",
            data: { pageTitle: 'Class' },

            controller: "ReportCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Report/Report.js',
                        ]
                    });
                }]
            }
        })

                // GroupSetting
        .state('GroupSetting', {
            url: "/GeneralSetting/Group",
            templateUrl: "/Home/GenericView?name=GeneralSetting/Group",
            data: { pageTitle: 'GroupSetting' },
            controller: "GroupSettingCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/GeneralSetting/Group.js',
                        ]
                    });
                }]
            }
        })

          .state('Group', {
              url: "/Group/Group",
              templateUrl: "/Home/GenericView?name=Group/Group",
              data: { pageTitle: 'Group' },
              controller: "GroupCtrl",
              resolve: {
                  deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load({
                          name: 'NWEGARApp',
                          insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                          files: [
                              '../Views/Group/Group.js',
                          ]
                      });
                  }]
              }
          })


        // LectureSetting
        .state('Lecture', {
            url: "/Lecture/Lecture",
            templateUrl: "/Home/GenericView?name=Lecture/Lecture",
            data: { pageTitle: 'Lecture' },
            controller: "LectureCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Lecture/Lecture.js',
                        ]
                    });
                }]
            }
        })
         // Student
        .state('Student', {
            url: "/Student/Student",
            templateUrl: "/Home/GenericView?name=Student/Student",
            data: { pageTitle: 'Student' },
            controller: "StudentCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Student/Student.js',
                        ]
                    });
                }]
            }
        })


                 // TeacherAssignment
        .state('TeacherAssignment', {
            url: "/Teacher/TeacherAssignment",
            templateUrl: "/Home/GenericView?name=Teacher/TeacherAssignment",
            data: { pageTitle: 'TeacherAssignment' },
            controller: "TeacherAssignmentCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Teacher/TeacherAssignment.js',
                        ]
                    });
                }]
            }
        })
        // Attendance
        .state('Attendance', {
            url: "/Attendance/Attendance",
            templateUrl: "/Home/GenericView?name=Attendance/Attendance",
            data: { pageTitle: 'Attendance' },
            controller: "AttendanceCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Attendance/Attendance.js',
                        ]
                    });
                }]
            }
        })
    
         // TeacherMonitoring
        .state('TeacherMonitoring', {
            url: "/Monitoring/TeacherMonitoring",
            templateUrl: "/Home/GenericView?name=Monitoring/TeacherMonitoring",
            data: { pageTitle: 'TeacherMonitoring' },
            controller: "TeacherMonitoringCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Monitoring/TeacherMonitoring.js',
                        ]
                    });
                }]
            }
        })


                 // ManagerMonitoring
        .state('ManagerMonitoring', {
            url: "/Monitoring/ManagerMonitoring",
            templateUrl: "/Home/GenericView?name=Monitoring/ManagerMonitoring",
            data: { pageTitle: 'ManagerMonitoring' },
            controller: "ManagerMonitoringCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Monitoring/ManagerMonitoring.js',
                        ]
                    });
                }]
            }
        })

       // UserListPage
       .state('User', {
           url: "/User/UserListPage",
           templateUrl: "/Home/GenericView?name=User/UserListPage",
           data: { pageTitle: 'User' },
           controller: "UserListPageCtrl",
           resolve: {
               deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                   return $ocLazyLoad.load({
                       name: 'MetronicApp',
                       insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                       files: [
                           '../Views/User/UserListPage.js',
                       ]
                   });
               }]
           }
       })

        .state('guest_queue_list', {
            url: "/guest/queueList",
            templateUrl: "/Home/GenericView?name=Guest/QueueList",
            data: { pageTitle: 'Admin Dashboard Template' },
            controller: "QueueListCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Guest/QueueList.js',
                        ]
                    });
                }]
            }
        })


        // Member Register
        .state('member_register', {
            url: "/member/register",
            templateUrl: "/Home/GenericView?name=Member/MemberRegister",
            data: { pageTitle: 'Admin Dashboard Template' },
            controller: "MemberRegisterCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../Views/Member/MemberRegister.js',
                        ]
                    });
                }]
            }
        })

        .state('member_list', {
            url: "/member/list",
            templateUrl: "/Home/GenericView?name=Member/MemberList",
            data: { pageTitle: 'Admin Dashboard Template' },
            controller: "MemberListCtrl",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            "../assets/global/plugins/datatables/datatables.min.css",
                            "../assets/global/plugins/datatables/datatables.all.min.js",
                            "../assets/global/plugins/datatables/datatables.min.js",
                            "../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
                            "../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
                            "../assets/pages/scripts/table-datatables-fixedheader.min.js",
                            "../assets/pages/scripts/table-datatables-responsive.min.js",

                            "../assets/global/scripts/datatable.min.js",
                            '../Views/Member/MemberList.js',
                        ]
                    });
                }]
            }
        })


        // AngularJS plugins
        .state('fileupload', {
            url: "/file_upload.html",
            templateUrl: "views/file_upload.html",
            data: { pageTitle: 'AngularJS File Upload' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '../assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    }, {
                        name: 'NWEGARApp',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Select
        .state('uiselect', {
            url: "/ui_select.html",
            templateUrl: "views/ui_select.html",
            data: { pageTitle: 'AngularJS Ui Select' },
            controller: "UISelectController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'NWEGARApp',
                        files: [
                            'js/controllers/UISelectController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Bootstrap
        .state('uibootstrap', {
            url: "/ui_bootstrap.html",
            templateUrl: "views/ui_bootstrap.html",
            data: { pageTitle: 'AngularJS UI Bootstrap' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'NWEGARApp',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Tree View
        .state('tree', {
            url: "/tree",
            templateUrl: "views/tree.html",
            data: { pageTitle: 'jQuery Tree View' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/jstree/dist/themes/default/style.min.css',

                            '../assets/global/plugins/jstree/dist/jstree.min.js',
                            '../assets/pages/scripts/ui-tree.min.js',
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Form Tools
        .state('formtools', {
            url: "/form-tools",
            templateUrl: "views/form_tools.html",
            data: { pageTitle: 'Form Tools' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            '../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            '../assets/global/plugins/typeahead/typeahead.css',

                            '../assets/global/plugins/fuelux/js/spinner.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            '../assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            '../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            '../assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            '../assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            '../assets/global/plugins/typeahead/handlebars.min.js',
                            '../assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            '../assets/pages/scripts/components-form-tools-2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Date & Time Pickers
        .state('pickers', {
            url: "/pickers",
            templateUrl: "views/pickers.html",
            data: { pageTitle: 'Date & Time Pickers' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/clockface/css/clockface.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            '../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/clockface/js/clockface.js',
                            '../assets/global/plugins/moment.min.js',
                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                            '../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Custom Dropdowns
        .state('dropdowns', {
            url: "/dropdowns",
            templateUrl: "views/dropdowns.html",
            data: { pageTitle: 'Custom Dropdowns' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Advanced Datatables
        .state('datatablesAdvanced', {
            url: "/datatables/managed.html",
            templateUrl: "views/datatables/managed.html",
            data: { pageTitle: 'Advanced Datatables' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/datatables/datatables.min.css',
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',

                            '../assets/pages/scripts/table-datatables-managed.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // Ajax Datetables
        .state('datatablesAjax', {
            url: "/datatables/ajax.html",
            templateUrl: "views/datatables/ajax.html",
            data: { pageTitle: 'Ajax Datatables' },
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/datatables/datatables.min.css',
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/scripts/datatable.js',

                            'js/scripts/table-ajax.js',
                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // User Profile
        .state("profile", {
            url: "/profile",
            templateUrl: "views/profile/main.html",
            data: { pageTitle: 'User Profile' },
            controller: "UserProfileController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/pages/css/profile.css',

                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                            '../assets/pages/scripts/profile.min.js',

                            'js/controllers/UserProfileController.js'
                        ]
                    });
                }]
            }
        })

        // User Profile Dashboard
        .state("profile.dashboard", {
            url: "/dashboard",
            templateUrl: "views/profile/dashboard.html",
            data: { pageTitle: 'User Profile' }
        })

        // User Profile Account
        .state("profile.account", {
            url: "/account",
            templateUrl: "views/profile/account.html",
            data: { pageTitle: 'User Account' }
        })

        // User Profile Help
        .state("profile.help", {
            url: "/help",
            templateUrl: "views/profile/help.html",
            data: { pageTitle: 'User Help' }
        })

        // Todo
        .state('todo', {
            url: "/todo",
            templateUrl: "views/todo.html",
            data: { pageTitle: 'Todo' },
            controller: "TodoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NWEGARApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/apps/css/todo-2.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            '../assets/apps/scripts/todo-2.min.js',

                            'js/controllers/TodoController.js'
                        ]
                    });
                }]
            }
        })

}]);

/* Init global settings and run the app */
NWEGARApp.run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);