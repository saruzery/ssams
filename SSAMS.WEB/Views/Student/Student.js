﻿angular.module('NWEGARApp').controller('StudentCtrl', function ($scope, NGService, $timeout) {
    $scope.filter = { ItemCountInPage: 20 };
    $scope.model = { ItemList: [] };
    $scope.Student = {};

    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.modelClass = { ItemList: [] };

    $scope.filterRoom = { ItemCountInPage: -1 };
    $scope.modelRoom = { ItemList: [] };

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 1)
            $scope.filter.FromDate = null;


        else if (type == 2)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.SearchText+filter.SearchText1+filter.SearchText2", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Cancel = function () {
        $scope.Student = {};
    }

    $scope.ExportStudentList = function (exportType) {
        var passData = {  exportType: exportType, filter: $scope.filter };
        var modalInstance = NGService.OpenModal({
            Url: 'Student/ExportStudentList', Size: 'lg', Data: passData, Backdrop: true
        });
        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    }

    $scope.Save = function () {
        NGService.CallService("Student.Save", $scope.Student).then(function (response) {
            $scope.GetList();
            $scope.Student = {};
        }, function (response) {
        });
    };


    $scope.Edit = function (Student) {
        $scope.Student = angular.copy(Student);
    }

    $scope.GetClass = function () {
        NGService.CallService("Class.GetList", $scope.filterClass).then(function (response) {
            $scope.modelClass = response.Data;
        }, function (response) {
        });
    };

    $scope.GetRoomByClassId = function (Student) {
        NGService.CallService("Class.GetRoomByClassId", { Id: Student.ClassId }).then(function (response) {
            $scope.modelRoom = response.Data;
        }, function (response) {
        });
    };

    $scope.Remove = function (id) {
        NGService.CallService("Student.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.GetList = function () {
        NGService.CallService("Student.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetClass();
    $scope.GetList();

});