﻿angular.module('NWEGARApp').controller('ExportStudentListCtrl', function ($scope, NGService, $timeout, $uibModalInstance, passData) {

    $scope.model = { ItemList: [] };
    $scope.filter = { ItemCountInPage: -1 };
 


    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        return function (table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
            window.location.href = uri + base64(format(template, ctx))
        }
    })();
    var init = function () {
        if (passData) {
            $scope.filter = passData.filter;
            $scope.filter.ItemCountInPage = -1;
            $scope.exportType = passData.exportType;
            $scope.GetList();
        }
    }

    $scope.download = function () {
            tableToExcel('testTable', '');
    }
  
    $scope.GetList = function () {
        NGService.CallService("Student.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

   
    $scope.close = function () {
        $uibModalInstance.dismiss();
    }
    init();
});