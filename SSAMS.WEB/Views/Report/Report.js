﻿angular.module('NWEGARApp').controller('ReportCtrl', function ($scope, NGService, $timeout) {
  
    $scope.model = { ItemList: [] };
    $scope.filter = { IsExpired:true, ItemCountInPage: 10 };
    $scope.Status = {}
    $scope.GroupDetail = {};

    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText+filter.SearchText1+filter.SearchTex2", function (oldValue, newValue) {
        $scope.GetList();
    });

    $scope.GetList = function () {
        NGService.CallService("Group.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;

        }, function (response) {
        });
    };

    $scope.MoreDetail = function (Group) {
        NGService.CallService("Group.GetListByGroupId", { Id: Group }).then(function (response) {
            var data = response.Data;
            for (var i = 0; i < $scope.model.ItemList.length; i++) {
                if ($scope.model.ItemList[i].Id == Group) {
                    $scope.model.ItemList[i]['GroupDetail'] = data;
                }
            }
        }, function (response) {

        });
    }
    /*
    $scope.GetStatus = function (Group) {
        NGService.CallService("Group.GetStatus", { Id: Group }).then(function (response) {
            $scope.Status = response.Data;
            // $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };
    */
    $scope.Edit = function (GroupDetail, Status, Group) {

        NGService.CallService("GroupDetail.EditGroupDetail", { Id: GroupDetail.Id, Status: Status }).then(function (response) {
            $scope.GroupDetail = response.Data;
            // $scope.GetList();
            $scope.MoreDetail(Group.Id);
        }, function (response) {
        });

    }

    $scope.GetList();


});