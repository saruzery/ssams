﻿angular.module('NWEGARApp').controller('UserListPageCtrl', function ($scope, NGService, $timeout) {

    $scope.model = { ItemList: [] };
    $scope.filter = { ItemCountInPage: 25 };
    $scope.User = {};

    $scope.OpenAddUser = function (data) {
        var passData = {};
        if (data)
            passData = data;
        var modalInstance = NGService.OpenModal({ Url: 'User/UserSavePage', Size: 'lg', Data: passData, Backdrop: true });
        modalInstance.result.then(function (selectedItem) {
            $scope.GetList();
        }, function () {
        });
    }

   /* $scope.Edit = function (User) {
        $scope.OpenAddUser(User);
    }*/
    $scope.Edit = function (User) {
        $scope.User = angular.copy(User);
    }

    $scope.Save = function () {
        NGService.CallService("User.Save", $scope.User, ".portlet").then(function (response) {
            $scope.GetList();
            $scope.clean();
        }, function (data) {
            console.log(data);
        });
    }

    $scope.clean = function () {
        $scope.User = {};
    }
    $scope.GetList = function () {
        NGService.CallService("User.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (data) {
        });
    }
    $scope.Remove = function (id) {
        NGService.CallService("User.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (data) {
            console.log(data);
        });
    }

    $scope.GetList();
});