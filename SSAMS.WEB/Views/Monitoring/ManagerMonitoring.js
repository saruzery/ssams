﻿angular.module('NWEGARApp').controller('ManagerMonitoringCtrl', function ($scope, NGService, $timeout) {

  
    $scope.filter = { ItemCountInPage: 5 };
    $scope.model = { ItemList: [] };
    $scope.filterMembers = { ItemCountInPage: 5 };
    $scope.modelMembers = { ItemList: [] };
    $scope.Manager = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 1)
            $scope.filter.FromDate = null;


        else if (type == 2)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Cancel = function () {
        $scope.Manager = {};
    }

    $scope.Save = function () {
        NGService.CallService("Manager.Save", $scope.Manager).then(function (response) {
            $scope.GetList();
            $scope.Manager = {};
        }, function (response) {

        });
    };

    $scope.SetIsActive = function (id) {
        NGService.CallService("Manager.SetIsActive", { Id: id }).then(function (response) {
            $scope.GetList();
            $scope.Manager = {};
        }, function (response) {
        });



        NGService.CallService("Manager.Save", $scope.Manager).then(function (response) {
            $scope.GetList();
            $scope.Manager = {};
        }, function (response) {

        });
    }


    $scope.Edit = function (Manager) {
        $scope.Manager = angular.copy(Manager);
    }


    $scope.Remove = function (id) {
        NGService.CallService("Manager.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.GetList = function () {
        NGService.CallService("Manager.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetManagerMembersList = function () {
        NGService.CallService("Manager.GetManagerMembersList", $scope.filterMembers).then(function (response) {
            $scope.modelMembers = response.Data;
            $scope.filterMembers.PageNumber = $scope.modelMembers.SelectedPage;
        }, function (response) {
        });
    };

    $scope.TeamMembersPopUp = function (data) {
        var passData = {};
        if (data)
            passData = data;
        var modalInstance = NGService.OpenModal({ Url: 'Manager/MemberPopUp', Size: 'lg', Data: passData, Backdrop: true });
        $scope.GetManagerMembersList();
        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    }
});