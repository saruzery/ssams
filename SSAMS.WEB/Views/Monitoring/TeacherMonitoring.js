﻿angular.module('NWEGARApp').controller('TeacherMonitoringCtrl', function ($scope, NGService, $timeout) {

    $scope.filter = { ItemCountInPage: 5 };
    $scope.model = { ItemList: [] };

    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.filterGroup = { ItemCountInPage: -1 };

    $scope.modelClass = { ItemList: [] };
    $scope.modelGroup = { ItemList: [] };

    $scope.filterMembers = { ItemCountInPage: 5 };
    $scope.modelMembers = { ItemList: [] };
    $scope.Attendance = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************

    $scope.clear = function (type) {
        if (type == 1)
            $scope.filter.FromDate = null;


        else if (type == 2)
            $scope.filter.ToDate = null;

    };

    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Cancel = function () {
        $scope.Attendance = {};
    }

    $scope.GetClass = function () {
        NGService.CallService("Class.GetList", $scope.filterClass).then(function (response) {
            $scope.modelClass = response.Data;
        }, function (response) {
        });
    };

    $scope.GetGroup = function () {
        NGService.CallService("Group.GetList", $scope.filterGroup).then(function (response) {
            $scope.modelGroup = response.Data;
        }, function (response) {
        });
    };

    $scope.GetList = function () {
        NGService.CallService("TeacherMonitoring.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };


    $scope.GetList();
    $scope.GetClass();
    $scope.GetGroup();


});