﻿/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

MetronicApp.factory('NGService', function ($uibModal, $q, $http,/* toastr,*/ $timeout) {
    var NGService = {};
    var localUrl = "/Home/CallHelper?Method=";
    var loginUrl = "/Account";
    
    NGService.CallService = function (helperMethod, postData, blockName) {

        var tag = 'body';
        if (blockName) {
            tag = blockName;
        }
        App.blockUI({
            target: $(tag), animate: true,
            //overlayColor: 'none'
        });

        var d = $q.defer();

        $http({
            method: 'POST', data: { parms: JSON.stringify(postData) } , url:  helperMethod
        }).then(function (respons, status) {
            console.log(respons);
            var response = respons.data;
            var tip = response.Type;
            if (response.HasError && response.HasError == true) {
                /*if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                } else {*/
                var alert = {};
                alert.type = tip == 'SUCCESS' ? 'success' : tip == 'FAIL' ? 'error' : '';
                alert.msg = response.Message;
                //$rootScope.AddAlert(alert);
                response.Data = eval('(' + response.Data + ')');
                //if (response.Message)
                /*if (alert.type == 'success')
                    toastr.success(response.Message);
                else
                    //toastr.error(response.Message);*/
                d.reject(response);
                //}
            } else {
                if (tip == 'SUCCESS' || tip == 'SUCCESS_WITH_DATA') {
                    //if (tip == 'SUCCESS')
                    // $rootScope.AddAlert({type:'success', msg:response.Message});

                    response.Data = eval('(' + response.Data + ')');
                    //if (response.Message)
                    //toastr.success(response.Message);
                    d.resolve(response);
                }/* else if (tip == 'NOT_AUTHENTICATED' || tip == 'NOT_AUTHORIZED' || tip == "NOT_VALID") {
                    window.location = loginUrl;
                }*/ else {
                    // if (response.Message)
                    //toastr.error(response.Message);
                    //$rootScope.AddAlert({ type: 'error', msg: response.Message });
                    response.Data = eval('(' + response.Data + ')');
                    d.reject(response);
                }
            }
            $timeout(function () {
                App.unblockUI(tag);
            }, 500);

        }, function (response) {
            d.reject(response.data);

            $timeout(function () {
                App.unblockUI(tag);
            }, 500);
        });
        return d.promise;
    }

    return NGService;
})


/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/account/login");  
    $stateProvider

            // Account
            .state('account', {
                url: "/account",
                templateUrl: "/Home/GenericView?name=Account/Login/Index",            
                data: {pageTitle: 'Admin Dashboard Template'},
                //controller: "DashboardCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                            '../assets/pages/css/login.min.css'
                               /* '../assets/global/plugins/morris/morris.css',                            
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',                            
                                '../assets/global/plugins/jquery.sparkline.min.js',

                                '../assets/pages/scripts/dashboard.min.js',
                                '../Views/Home/Dashboard.js',*/
                            ] 
                        });
                    }]
                }
            })
     // Login
            .state('account.login', {
                url: "/login",
                templateUrl: "/Home/GenericView?name=Account/Login/Login",
                data: { pageTitle: 'Admin Dashboard Template' },
                controller: "LoginCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                               '../Views/Account/Login/Login.js'
                            ]
                        });
                    }]
                }
            })

    // Login
            .state('account.forget', {
                url: "/login",
                templateUrl: "/Home/GenericView?name=Account/Login/ForgetPassword",
                data: { pageTitle: 'Admin Dashboard Template' },
                //controller: "DashboardCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                               /* '../assets/global/plugins/morris/morris.css',                            
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',                            
                                '../assets/global/plugins/jquery.sparkline.min.js',

                                '../assets/pages/scripts/dashboard.min.js',
                                '../Views/Home/Dashboard.js',*/
                            ]
                        });
                    }]
                }
            })

    // Login
            .state('lock', {
                url: "/lock",
                templateUrl: "/Home/GenericView?name=Account/Lock",
                data: { pageTitle: 'Admin Dashboard Template' },
                //controller: "DashboardCtrl",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/pages/css/lock.min.css'
                               /* '../assets/global/plugins/morris/morris.css',                            
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',                            
                                '../assets/global/plugins/jquery.sparkline.min.js',

                                '../assets/pages/scripts/dashboard.min.js',
                                '../Views/Home/Dashboard.js',*/
                            ]
                        });
                    }]
                }
            })


}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "$state", function ($rootScope, $state) {
    $rootScope.$state = $state; // state to be accessed from view
   // $rootScope.$settings = settings; // state to be accessed from view
}]);