angular.module('NWEGARApp').controller('DashboardCtrl', function ($rootScope, $scope, NGService, $timeout) {

    $scope.model = { IsExpired: false, ItemList: [] };
    $scope.filter = { ItemCountInPage: 10 };


    $scope.GetList = function () {
        NGService.CallService("Group.GetByUserId", {}).then(function (response) {
            $scope.model = response.Data;
          //  $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };


    $scope.OpenStudentListPopUp = function (data) {
        var passData = {};
        if (data)
            passData = data;
        var modalInstance = NGService.OpenModal({ Url: 'Home/StudentListPopUp', Size: 'lg', Data: passData, Backdrop: true });
        modalInstance.result.then(function (selectedItem) {


        }, function () {
        });
    }


    $scope.GetList();
    
});