angular.module('NWEGARApp').controller('StudentListPopUpCtrl', function ($scope, NGService, $uibModalInstance, $timeout, passData) {

    $scope.StudentList = [];
    $scope.GroupDetail = {};

    var init = function () {
        if (passData && passData.Id)
            $scope.GetListByGroupId(passData.Id);
    }

    $scope.Close = function () {
        $uibModalInstance.dismiss();
    };

    $scope.GetListByGroupId = function (Group) {
        NGService.CallService("Group.GetListByGroupId", { Id: Group }).then(function (response) {
            $scope.StudentList = response.Data;
        }, function (response) {

        });
    }

    $scope.Edit = function (StudentList, Status) {
       
        NGService.CallService("GroupDetail.EditGroupDetail", { Id: StudentList.Id, Status: Status }).then(function (response) {
            $scope.GroupDetail = response.Data;
            init();
        }, function (response) {
        });

    }


    init();
    
});