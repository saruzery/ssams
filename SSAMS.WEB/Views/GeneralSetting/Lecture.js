﻿angular.module('NWEGARApp').controller('LectureCtrl', function ($scope, NGService, $timeout) {
    var LectureSettingOriginal = {};

    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.modelClass = { ItemList: [] };

    $scope.filter = { ItemCountInPage: 10 };
    $scope.model = { LectureSettingList: [] };

    $scope.LectureSetting = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 2)
            $scope.filter.FromDate = null;


        else if (type == 3)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Remove = function (id) {
        NGService.CallService("LectureSetting.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.Cancel = function () {
        $scope.LectureSetting = {};
    }
    $scope.Edit = function (LectureSetting) {
        $scope.LectureSetting = angular.copy(LectureSetting);
    }

    $scope.Save = function () {
        NGService.CallService("LectureSetting.Save", $scope.LectureSetting).then(function (response) {
            $scope.GetList();
            $scope.LectureSetting = {};
        }, function (response) {

        });
    };

    $scope.GetClassSetting = function () {
        NGService.CallService("ClassSetting.GetList", $scope.filterClassSetting).then(function (response) {
            $scope.modelClassSetting = response.Data;
        }, function (response) {
        });
    };

    $scope.GetList = function () {
        NGService.CallService("LectureSetting.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetList();
    $scope.GetClassSetting();

});