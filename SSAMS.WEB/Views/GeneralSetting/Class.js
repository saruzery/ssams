﻿angular.module('NWEGARApp').controller('ClassCtrl', function ($scope, NGService, $timeout) {
    $scope.filter = { ItemCountInPage: 5 };
    $scope.model = { ItemList: [] };
    $scope.ClassSetting = {};

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 1)
            $scope.filter.FromDate = null;


        else if (type == 2)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Cancel = function () {
        $scope.ClassSetting = {};
    }

    $scope.Save = function () {
        NGService.CallService("ClassSetting.Save", $scope.ClassSetting).then(function (response) {
            $scope.GetList();
            $scope.ClassSetting = {};
        }, function (response) {
        });
    };


    $scope.Edit = function (ClassSetting) {
        $scope.ClassSetting = angular.copy(ClassSetting);
    }


    $scope.Remove = function (id) {
        NGService.CallService("ClassSetting.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.GetList = function () {
        NGService.CallService("ClassSetting.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
        }, function (response) {
        });
    };

    $scope.GetList();
});