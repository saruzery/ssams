﻿angular.module('NWEGARApp').controller('GroupSettingCtrl', function ($scope, NGService, $timeout) {
    var GroupSettingOriginal = {};

    $scope.filterClassSetting = { ItemCountInPage: -1 };
    $scope.filterStudentSetting = { ItemCountInPage: -1 };
    $scope.filterLectureSetting = { ItemCountInPage: -1 };
    $scope.filterTeacherSetting = { ItemCountInPage: -1 };

    $scope.modelClassSetting = { ItemList: [] };
    $scope.modelStudentSetting = { ItemList: [] };
    $scope.modelLectureSetting = { ItemList: [] };
    $scope.modelTeacherSetting = { ItemList: [] };

    $scope.filter = { ItemCountInPage: 10 };
    $scope.model = { GroupSettingList: [] };
    $scope.GroupSetting = {};
    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 2)
            $scope.filter.FromDate = null;


        else if (type == 3)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************
    $scope.Remove = function (id) {
        NGService.CallService("GroupSetting.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.Cancel = function () {
        $scope.GroupSetting = {};
    }
    $scope.Edit = function (GroupSetting) {
        $scope.GroupSetting = angular.copy(GroupSetting);
    }

    $scope.Save = function () {
        NGService.CallService("GroupSetting.Save", $scope.GroupSetting).then(function (response) {
            $scope.GetList();
            $scope.GroupSetting = {};
        }, function (response) {

        });
    };

    $scope.GetClassSetting = function () {
        NGService.CallService("ClassSetting.GetList", $scope.filterClassSetting).then(function (response) {
            $scope.modelClassSetting = response.Data;
        }, function (response) {
        });
    };

    $scope.GetStudentSetting = function () {
        NGService.CallService("Student.GetList", $scope.filterStudentSetting).then(function (response) {
            $scope.modelStudentSetting = response.Data;
        }, function (response) {
        });
    };


    $scope.GetLectureSetting = function () {
        NGService.CallService("LectureSetting.GetList", $scope.filterLectureSetting).then(function (response) {
            $scope.modelLectureSetting = response.Data;
        }, function (response) {
        });
    };


    $scope.GetTeacherSetting = function () {
        NGService.CallService("User.GetList", $scope.filterTeacherSetting).then(function (response) {
            $scope.modelTeacherSetting = response.Data;
        }, function (response) {
        });
    };


    $scope.GetList = function () {
        NGService.CallService("GroupSetting.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetList();
    $scope.GetClassSetting();
    $scope.GetTeacherSetting();
    $scope.GetLectureSetting();
    $scope.GetStudentSetting();
});