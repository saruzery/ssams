﻿angular.module('NWEGARApp').controller('ClassCtrl', function ($scope, NGService, $timeout) {
    $scope.filter = { ItemCountInPage: 5 };
    $scope.model = { ItemList: [] };
    $scope.Class = {};
    $scope.ClassDetailList = [];
    $scope.ClassDetail = {};

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 1)
            $scope.filter.FromDate = null;


        else if (type == 2)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************

    $scope.Delete = function (index) {
        $scope.ClassDetailList.splice(index, 1);
    }

    $scope.AddRoom = function () {
        var data = angular.copy($scope.ClassDetail);
        $scope.ClassDetailList.push(data);
        $scope.ClassDetail = {};

    }

    $scope.Cancel = function () {
        $scope.Class = {};
    }

    $scope.Save = function () {
        NGService.CallService("Class.Save", { Class: $scope.Class, ClassDetail: $scope.ClassDetailList }).then(function (response) {
            $scope.GetList();
            $scope.Class = {};
            $scope.ClassDetailList = [];
        }, function (response) {
        });
    };

    $scope.MoreDetail = function (Class) {
        NGService.CallService("Class.GetListByClassId", { Id: Class }).then(function (response) {
            var data = response.Data;
            for (var i = 0; i < $scope.model.ItemList.length; i++) {
                if ($scope.model.ItemList[i].Id == Class) {
                    $scope.model.ItemList[i]['ClassDetail'] = data;
                }
            }
        }, function (response) {
        });
    }


    $scope.Edit = function (Class) {
        NGService.CallService("Class.GetById", { Id: Class.Id }).then(function (response) {
            $scope.Class = response.Data;
            $scope.GetClass(response.Data);
            $scope.IsEdit = true;
        }, function (response) {
        });
        NGService.CallService("Class.GetListByClassId", { Id: Class.Id }).then(function (response) {
            $scope.ClassDetailList = response.Data;
        }, function (response) {
        });
    }


    $scope.Remove = function (id) {
        NGService.CallService("Class.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.GetList = function () {
        NGService.CallService("Class.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };

    $scope.GetList();
});