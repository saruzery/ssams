﻿angular.module('NWEGARApp').controller('GroupCtrl', function ($scope, NGService, $timeout) {
    // var GroupOriginal = {};
    $scope.filter = { ItemCountInPage: 10 };
    $scope.filterClass = { ItemCountInPage: -1 };
    $scope.filterStudent = { ItemCountInPage: -1 };
    $scope.filterLecture = { ItemCountInPage: -1 };
    $scope.filterTeacher = { ItemCountInPage: -1 };

    $scope.modelClass = { ItemList: [] };
    $scope.modelStudent = [];
    $scope.modelLecture = { ItemList: [] };
    $scope.modelTeacher = { ItemList: [] };
    $scope.model = { IsExpired: false, ItemList: [] };
    $scope.IsEdit = false;
    $scope.Group = {};
    $scope.Class = {};
    $scope.Student = {};
    $scope.GroupDetailList = [];
    $scope.RoomStudents = [];

    $scope.GroupDetail = {};

    $scope.filterRoom = { ItemCountInPage: -1 };
    $scope.modelRoom = { ItemList: [] };

    $scope.StartHour = {};
    $scope.StartMin = {};

    $scope.todate = new Date();

    //Start Search Functions*************************************************************************************
    $scope.pickers = [];
    $scope.open = function (index) {
        for (var i = 0; i < $scope.pickers.length; i++) {
            $scope.pickers[i] = false;
        }
        $scope.pickers[index] = true;
    }
    $scope.clear = function (type) {
        if (type == 2)
            $scope.filter.FromDate = null;


        else if (type == 3)
            $scope.filter.ToDate = null;

    };
    $scope.$watch("filter.FromDate+filter.ToDate+filter.SearchText+filter.SearchText1+filter.SearchTex2", function (oldValue, newValue) {
        $scope.GetList();
    });
    //End Search Functions**************************************************************************************

    $scope.GetRoomAndLecutreByClassId = function (obj) {
        NGService.CallService("Class.GetRoomByClassId", { Id: obj.Class.Id }).then(function (response) {
            $scope.modelRoom = response.Data;
        }, function (response) {
        });
        NGService.CallService("Lecture.GetByClassId", {Id: obj.Class.Id}).then(function (response) {
            $scope.modelLecture = response.Data;
        }, function (response) {
        });
    };

    $scope.getStartHour = function () {
        for (var i = 1; i < 25; i++) {
            $scope.StartHour[i] = i;
        }
        $scope.StartHour
    }

    $scope.getStartMin = function () {

        for (var i = 0; i < 61; i= i+5) {
            $scope.StartMin[i] = i;
        }
        $scope.StartMin
    }
    $scope.Save = function () {
        NGService.CallService("Group.Save", { Group: $scope.Group, GroupDetail: $scope.GroupDetailList }).then(function (response) {
            $scope.GetList();
            $scope.Cancel();
        }, function (response) {
        });
    };

    $scope.MoreDetail = function (Group) {
        NGService.CallService("Group.GetListByGroupId", { Id: Group }).then(function (response) {
            var data = response.Data;
            for (var i = 0; i < $scope.model.ItemList.length; i++) {
                if ($scope.model.ItemList[i].Id == Group) {
                    $scope.model.ItemList[i]['GroupDetail'] = data;
                }
            }
        }, function (response) {

        });
    }

    $scope.Remove = function (id) {
        NGService.CallService("Group.Remove", { Id: id }).then(function (response) {
            $scope.GetList();
        }, function (response) {
        });
    }

    $scope.Cancel = function () {
        $scope.Group = {};
        $scope.GroupDetail = {};
        $scope.GroupDetailList = {};
    }

    $scope.Edit = function (Group) {
        NGService.CallService("Group.GetById", { Id: Group.Id }).then(function (response) {
            $scope.Group = response.Data;
            $scope.GetClass(response.Data);

            $scope.IsEdit = true;
        }, function (response) {
        });
        NGService.CallService("Group.GetListByGroupId", { Id: Group.Id }).then(function (response) {
            $scope.GroupDetailList = response.Data;
        }, function (response) {
        });
    }



    $scope.AddStudent = function () {
        var data = angular.copy($scope.GroupDetail);
       
        if ($scope.GroupDetail.StudentDetail == undefined) {

            var p=$scope.modelStudent.length;
     
            for (var i = 0; i < p ; i++)
                $scope.GroupDetailList.push($scope.modelStudent[i]);
         
        } else {
            var data = angular.copy($scope.GroupDetail);
            data.StudentId = data.StudentDetail.Id;
            data.StudentName = data.StudentDetail.StudentName;
            data.Surname = data.StudentDetail.Surname;

            data.ClassId = data.Class.Id;
            data.ClassName = data.Class.ClassName;

            data.ClassDetailId = data.ClassDetail.Id;
            data.RoomName = data.ClassDetail.RoomName;

            $scope.GroupDetailList.push(data);
        }
        $scope.GroupDetail = {};
        $scope.RoomStudents = [];

    }

    $scope.Delete = function (index) {
        $scope.GroupDetailList.splice(index, 1);
    }

    $scope.GetStudent = function (obj) {
        NGService.CallService("Student.GetStudentByClassId", { Id: obj.ClassDetail.Id }).then(function (response) {
            $scope.modelStudent = response.Data;
        }, function (response) {
        });
    };

    $scope.GetClass = function (obj) {
        NGService.CallService("Class.GetList", {}).then(function (response) {
            $scope.modelClass = response.Data;
        }, function (response) {
        });
    };


    $scope.GetLecture = function () {
        NGService.CallService("Lecture.GetList", $scope.filterLecture).then(function (response) {
            $scope.modelLecture = response.Data;
        }, function (response) {
        });
    };

    $scope.GetTeacher = function () {
        NGService.CallService("User.GetList", $scope.filterTeacher).then(function (response) {
            $scope.modelTeacher = response.Data;
        }, function (response) {
        });
    };

    $scope.GetList = function () {
        NGService.CallService("Group.GetList", $scope.filter).then(function (response) {
            $scope.model = response.Data;
            $scope.filter.PageNumber = $scope.model.SelectedPage;
        }, function (response) {
        });
    };


    $scope.GetTeacher();
    //$scope.GetLecture();
    $scope.GetClass();
    $scope.getStartHour();
    $scope.getStartMin();









});
